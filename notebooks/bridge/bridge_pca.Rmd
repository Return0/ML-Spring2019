---
title: "Strain Analysis"
output: html_notebook
---

## Problem Description

The data measures the strain recorded on a bridge in 27 different locations. Each data point corresponds to the maximal recorded strain for the particular gauge during the truck crossing. The data contains 1509 or so truck events.

There are 2 datasets available:

1.) Processed_Events_2 

This is a MATLAB file that is {1929,4} array where the 1929 rows are individual truck events containing the year/month/day of the event followed by a table (approx. 450 x 42) which contains the strain readings over many time steps throughout the duration of the truck event. In this set, there are 42 strain gauges; some of the SGs are at the same location, and thus the readings were averaged together to create the 27 SGs used in my research.

2.) InputOutput

This contains the sorted input/output strains at each location. X represents the 26 input strains used to predict the corresponding Y. 1509 training events are used in X while the 420 testing events are found in X_trial. If you need the files sorted in a different manner to run the PCA, just let me know and I can probably easily figure it out in MATLAB.

## Input Output Data

Load data from a MATLAB matrix file.
```{r}
data <- R.matlab::readMat("InputOutput.mat")
```

- `data$X` and `data$Y` contain the training event datasets. Each element of this list includes 26 of the train gauge data, and the 27th is in the corresponding element of data$Y
- `data$Xtrial` and `data$Ytrial` contain the test event datasets.


Create a dataset that contains all strain readings. Each row is a truck and each column is a strain gauge reading.
```{r}
# take the values of other strains when girder 1 is at the maximum
events <- t(rbind(data$Y[[1]][[1]], data$X[[1]][[1]]))
# take the maxima
#events <- t(do.call(rbind,lapply(1:27, function(i){data$Y[[i]][[1]]})))
events.df <- as.data.frame(events)
events.df$time <- 1:nrow(events.df)
```
The `seq` column represents the number of the event. Assuming here that the truck events are sorted chronologically.


Run principal component analysis on the data and plot all the events:
```{r}
comps <- prcomp(~. - time,data=events.df, center=TRUE, scale=FALSE)
```

```{r}
library(ggfortify)
plt <- autoplot(comps, data=events.df, colour='time', loadings=TRUE, loadings.label=TRUE,
                loadings.colour="grey") + ggplot2::theme_light()
ggsave("trucks_pca.pdf", plot=plt, width=8, height=6)
plt
```


### Northbound and Southbound Events

The following message is from Kathryn (offset by 1 to account for adding the first strain gauge back in). Note that the columns and rows are swapped in my data (each row is a data point).

It is easier to tell the difference when looking at the events in InputOutput. X{1,1} shows the strain readings at the other 26 locations when Station 1 (G1-S2) is at its maximum for the truck event, which is represented  by Y{1,1}. For column 1 (event #1), Row 7 (G2 midspan) = 40 and Row 17 (G4 midspan) = 20, therefore it is a *southbound* event. For column 12 (event #12), Row 7 = 27 and Row 17 = 87, so this is a northbound event. In this InputOutput file, it is possible that they are not yet randomized - but later in the program they are. The events in Processed_Events_2 should be chronological.

This can be seen in this plot:
```{r}
plot(events.df$V7, events.df$V17)
abline(0,1,col="red")
```



```{r}
events.df$direction <- events.df$V7 > events.df$V17
```

```{r}
library(ggfortify)
plt <- autoplot(comps, data=events.df, colour='direction', loadings=TRUE, loadings.label=TRUE,
                loadings.colour="grey", loadings.label.colour="black") + ggplot2::theme_light()
ggsave("trucks_pca_dir.pdf", plot=plt, width=8, height=6)
plt
```