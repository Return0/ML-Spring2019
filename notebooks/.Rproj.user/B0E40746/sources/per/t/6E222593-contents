---
title: "Assignment 8: Pick 3+ Problems"
subtitle: CS 780/880 Machine Learning
output:
  pdf_document: default
  html_notebook: default
---

- **Cap**: 120% 
- **Due**: Friday April 27th at 10:00AM
- **Submisssion**: Turn in as a __PDF__ and the relevant __source code__ on [MyCourses](http://mycourses.unh.edu)
- **Questions**: [Piazza](https://piazza.com/unh/spring2018/cs780880/) and Office hours: _Marek_: Mon 3-4pm, _Bence_: Wed 2-3pm


# Problem 1 [33%]

Describe, in words, the results that you would expect if you performed K-means clustering of the eight shoppers in Figure 10.14 in ISL, on the basis of their sock and computer purchases, with K = 2. Give three answers, one for each of the variable scalings displayed. Explain.

# Problem 2 [33%]

In this problem, you will compute principal components for the `Auto` dataset. First remove qualitative features, which cannot be handled by PCA. Then:

1. Compute principal components without scaling features. Plot the result (you can use `ggfortify::autoplot`).
2. Compute principal components after scaling features to have unit variance. Plot the result (you can use `ggfortify::autoplot`).
3. How do the principal components computed in parts 1 and 2 compare?

# Problem 3 [33%]

*For this exercise, refer to Chapter 7 of Mitchel's Machine Learning.* 

Consider a learning problem with a single real-valued feature $X = \mathbb{R}$, and identical sets of hypotheses and concepts $\mathcal{C} = \mathcal{H}$. Hypotheses are a set of open intervals over the real numbers: 
$$ \mathcal{H} = \{ (a < x < b)~:~a,b \in \mathbb{R} \} ~.$$
What is the probability that a hypothesis consistent with $m$ examples of this target concept (that is the training error is 0) will have true error at least $\epsilon$? Solve this by computing the VC dimension of the hypothesis space.

# Problem 4 [33%]

*For this exercise, refer to Chapter 14 of the Russel & Norvig's Artificial Intelligence.* 

We have a bag of three biased coins $a,b,c$ with probabilities of coming up heads of $0.2, 0.6, 0.8$, respectively. One coin is drawn randomly from the bag (with equal likelihood of drawing each coin), and then the coin is flipped three times to generate outcomes $X_1$, $X_2$, and $X_3$.

1. Draw the Bayesian network corresponding to this setup and define the necessary CPT.
2. Calculate which coin was most likely to have been drawn from the bag if the observed flips come out heads twice and tails once. *Hint*: Need to consider only one permutation of flips.