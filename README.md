# CS750 / CS850: Machine Learning #

### When and Where 

Mon & Wed, 11:10 am - 12:40 pm in **Spaulding Life Sciences Hall G26**

Fri: *Possible recitation*

See [class overview](overview/overview.pdf) for the information on grading, rules, and office hours.

## Syllabus

Some slides are from last year. They will all be updated before the particular lecture. The topics are preliminary and may change (e.g. due to snow days) or due to popular demand.

Regarding *reading materials* see the section on textbooks below. *ISL* is the main textbook, *TBD* stands for "to be determined".


| Date   | Day | Slides                                                          | Reading       | Notebooks
| ------ | --- | -----------------------------                                   |------------   | ---------
| Jan 23 | Wed | [Introduction](slides/class1/class1.pdf)                        | ISL 1,2       | [(RMD)](notebooks/class1/introduction.Rmd) [(PDF)](notebooks/class1/introduction.pdf) [Plots](slides/figs/class1/plots.R)
| Jan 28 | Mon | [Linear regression I](slides/class2/class2.pdf)                 | ISL 3.1-2     | [Linear](intro_ml_17_files/linear_regression.Rmd)
| Jan 30 | Wed | [Linear regression II](slides/class3/class3.pdf)                | ISL 3.3-6     | [(RMD)](notebooks/class3/linear_regression.Rmd) [(PDF)](notebooks/class3/linear_regression.pdf)
| Feb 04 | Mon | [Logistic regression](slides/class4/class4.pdf)                 | ISL 4.1-3     | [Logistic](intro_ml_17_files/logistic_regression.Rmd)
| Feb 06 | Wed | Gradient descent for ML                                         |               | 
| Feb 11 | Mon | [LDA, QDA, Naive Bayes](slides/class5/class5.pdf) 		        | ISL 4.4-6   | [QQplot-rmd](notebooks/class3/qqplots.Rmd)  [QQplot-pdf](notebooks/class3/qqplots.pdf) [bitcoin-rmd](notebooks/example/bitcoin_price.Rmd) [bitcoin-pdf](notebooks/example/bitcoin_price.pdf) 
| Feb 13 | Wed | [Maximum likelihood](slides/class6/class6.pdf)                  |               | [ML](intro_ml_17_files/maxlikelihood.Rmd)              |
| Feb 18 | Mon | [Cross-validation](slides/class7/class7.pdf)                    | ISL 5         | [(RMD)](notebooks/class7/Overfitting.Rmd) [(PDF)](notebooks/class7/Overfitting.pdf)
| Feb 20 | Wed | [Model selection](slides/class8/class8.pdf)                     | ISL 6.1-6.2   |
| Feb 25 | Mon | [Dimensionality](slides/class9/class9.pdf)                      | ISL 6.3-6.4   |
| Feb 27 | Wed | [Regression splines](slides/class10/class10.pdf)                | ISL 7         |       |
| Mar 04 | Mon | [Midterm review](slides/class11/class11.pdf)                    |               |
| Mar 06 | Wed | Mini-project presentation                                       |               |
| Mar 11 | Mon | **Spring Break!**                                               |               |
| Mar 13 | Wed | **Spring Break!**                                               |               |
| Mar 18 | Mon | [Linear algebra](slides/class12/class12.pdf)                    | LAR           | [linear algebra](intro_ml_17_files/linearalgebra.Rmd)     |
| Mar 20 | Wed | [Linear algebra](slides/class12/class12.pdf)                    | LAR           |
| Mar 25 | Mon | [Decision trees and boosting](slides/class13.pdf)               | ISL 8         |       |
| Mar 27 | Wed | [SVM](slides/class14/class14.pdf)                               | ISL 9         |       |
| Apr 01 | Mon | [SVM, Project](slides/class15/class15.pdf)                      | ISL 9         |       |
| Apr 03 | Wed | [PCA](slides/class16/class16.pdf)                               | ISL 10.1-2    |  [PCA](intro_ml_17_files/pca.Rmd)  
| Apr 08 | Mon | [Clustering](slides/class17/class17.pdf)                        | ISL 10.3-5    |  [kmeans](intro_ml_17_files/kmeans.Rmd) 
| Apr 10 | Wed | [Learning theory]()                                             | ML 7, [Paper](http://www.kyb.mpg.de/fileadmin/user_upload/files/publications/pdfs/pdf2819.pdf) 
| Apr 15 | Mon | [Bayesian machine learning](slides/class19/class19.pdf)         | AIMA 14       |
| Apr 17 | Wed | [Bayesian machine learning](slides/class19/class19.pdf)         | AIMA 14       |
| Apr 22 | Mon | [Neural networks](slides/class20/class20.pdf)                   | DL            |
| Apr 24 | Wed | [Convolutional & Recurrent NN](slides/class21/class21.pdf) [More](slides/class21/class21_more.pdf)  | DL            |               | 
| Apr 29 | Mon | [Beyond the Intro](slides/class22/class22.pdf)                  | ISL           |
| May 01 | Wed | [Final exam review](intro_ml_17_files/class19.pdf)              |               |       |    
| May 06 | Mon | [Project presentations and discussion](slides/class24/class24.pdf) |            | 

## Assignments

*Office Hours*:

  - *Jordan*: Fri 10-11AM in W240
  - *Marek*: Wed 3-4pm in N215b

The dates below are tentative. 

| Assignment                      | Source                          |  Due Date          |
|-------------------------------- | ------------------------------- | -------------------|
| [0](assignments/assignment0.pdf)| [0](assignments/assignment0.Rmd)| 1/26/18 at 11:59PM |
| [1](assignments/assignment1.pdf)| [1](assignments/assignment1.Rmd)| 2/02/18 at 11:59PM |
| [2]()                           |                                 | 2/09/18 at 11:59PM |
| [3]()                           |                                 | 2/16/18 at 11:59PM |
| [4]()                           |                                 | 2/23/18 at 11:59PM |
| [5]()                           |                                 | 3/02/18 at 11:59PM |
| [6]()                           |                                 | 3/09/18 at 11:59PM |
| [7]()                           |                                 | 3/23/18 at 11:59PM |
| [8]()                           |                                 | 4/06/18 at 11:59PM |
| [9]()                           |                                 | 4/20/18 at 11:59PM |
| [10]()                          |                                 | 5/04/19 at 11:59PM |

## Solutions

Not yet.

## Projects

The projects will involve teams of at most **5** students. Please sign up on [mycourses](http://mycourses.unh.edu). The teams may change for each one of the two projects. The first project will be due before Spring break. The second (main) project will be due by the end of the semester.

For final project details please see [here](project/README.md). The preliminary due dates for this project are:
- 4/17: Preliminary report and code
- 5/03: Final report and code

## Quizzes

The projected dates for the quizzes are:

| Quiz                          | Dates Available|
| ----------------------------- | -------------- |
| Quiz 1                        | 2/11-2/15      |
| Quiz 2                        | 3/04-3/08      |
| Quiz 3                        | 4/08-4/12      |
| Quiz 4                        | 4/22-4/26      |

The quizzes will be available online on mycourses. See [practice questions](questions/test_questions.pdf) for the type of questions you may expect to be on the quizzes.

## Exams

The final exam is currently scheduled to be a take-home between 5/10-14. The exam will be pen and paper based with no programming.

## Textbooks ##

### Main Reference:
**ISL**: [James, G., Witten, D., Hastie, T., & Tibshirani, R. (2013). An Introduction to Statistical Learning](http://www-bcf.usc.edu/~gareth/ISL/)

### In-depth Machine Learning:
- **ESL**: [Hastie, T., Tibshirani, R., & Friedman, J. (2009). The Elements of Statistical Learning. Springer Series in Statistics (2nd ed.)](http://statweb.stanford.edu/~tibs/ElemStatLearn)
- **MLP**: Murphy, K (2012). Machine Learning, A Probabilistic Perspective.
- **PRM** Bishop, C. M. (2006). Pattern Recognition and Machine Learning. 
- **ML**: Mitchell, T. (1997). Machine Learning
- Scholkopf B., Smola A. (2001). Learning with Kernels.
- **DL**: Goodfellow, I., Bengio, Y., & Courville, A. (2016). [Deep Learning](http://www.deeplearningbook.org/) 

### Linear Algebra:
- **LAO**: Hefferon, J. [Linear Algebra](http://joshua.smcvt.edu/linearalgebra/#current_version) (2017)
- **LA**: Strang, G. [Introduction to Linear Algebra](http://math.mit.edu/~gs/linearalgebra/). (2016) *Also see:* [online lectures](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/)
- **LAR**: [Introductory Linear Agebra with R](http://bendixcarstensen.com/APC/linalg-notes-BxC.pdf)

### Mathematical Optimization:
- Boyd, S., & Vandenberghe, L. (2004). [Convex Optimization](http://web.stanford.edu/~boyd/cvxbook/)
- Nocedal, J., Wright, S. (2006). [Numerical Optimization](https://www.springer.com/us/book/9780387303031)

### Related Areas:
- **RL**: Sutton, R. S., & Barto, A. (2018). [Reinforcement learning](http://people.inf.elte.hu/lorincz/Files/RL_2006/SuttonBook.pdf). 2nd edition 
- **RLA**: Szepesvari, C. (2013), [Algorithms for Reinforcement Learning](https://sites.ualberta.ca/~szepesva/RLBook.html)
- **AIMA**: Russell, S., & Norvig, P. (2013). Artificial Intelligence A Modern Approach. (3rd ed.).

## R Resources

- [R For Data Science](https://r4ds.had.co.nz/index.html)
- [Cheatsheets](https://www.rstudio.com/resources/cheatsheets/)
- [R Markdown](https://github.com/rstudio/cheatsheets/raw/master/rmarkdown-2.0.pdf)


## Class Content

The goal of this class is to teach you how to use *machine learning* to *understand data* and *make predictions* in practice. The class will cover the fundamental concepts and algorithms in machine learning and data science as well as a wide variety of practical algorithms. The main topics we will cover are:

1. The maximum likelihood principle
- *Regression*: Linear regression
- *Classification*: Logistic regression and linear discriminant analysis
- *Cross-validation*: bootstrap, and over-fitting
- *Model selection*: Regularization, Lasso
- *Nonlinear models*: Decision trees, Support vector machines
- *Unsupervised*: Principal component analysis, k-means
- *Advanced topics*: Bayes nets and deep learning

The graduate version of the class will cover the same topics in more depth.

### Programming Language ###

The class will involve hand-on data analysis using machine learning methods. The recommended language for programming assignments is [R](https://www.r-project.org/) which is an excellent tool for statistical analysis and machine learning. *No prior knowledge of R is needed or expected*; the book and lecture will cover a gentle introduction to the language. *Experienced students may also choose other alternatives, such as Python or Matlab.*

We recommend using the free [R Studio](https://www.rstudio.com) for completing programming assignments. R Notebooks are very convenient for producing reproducible reports and we encourage you to use them. Jupyter is a similar alternative for Python.

**Python resources**: Book code is available for python, for example, [here](https://github.com/JWarmenhoven/ISLR-python). If you find other good sources, please let me know.

## Pre-requisites ##

Basic programming skills (scripting languages like Python are OK) and some familiarity with statistics and calculus. If in doubt, please email the instructor.
