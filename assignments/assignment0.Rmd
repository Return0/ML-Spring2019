---
title: "Assignment 0"
subtitle: CS 750/850 Machine Learning
output:
  pdf_document: default
  html_notebook: default
---

- **Due**: Saturday 1/26 at 11:59PM
- **Submisssion**: Turn in as a __PDF__ and the relevant __source code__ on [MyCourses](http://mycourses.unh.edu)
- **Questions**: [Piazza](https://piazza.com/unh/spring2019/cs750/) and Office hours: _Marek_: Wed 3-4pm, Jordan: Fri 10:00-11:00AM
- **Extra office hours**: Marek: Friday 9:00-10:00 AM on 1/25.

## Problem 1 [20%] 

What are the advantages and disadvantages of very flexible (vs less flexible) approach for regression or classification? 

1. When would be a more flexible approach preferable?
2. What about a less-flexible approach?

## Problem 2 [40\%]

Describe some real-life applications for machine learning. 

1. Describe one real-life application in which *classification* combined with *prediction* may be useful. Describe the response and predictors. 
2. Describe one real-life application in which *classification* combined with *inference* may be useful. Describe the response and predictors. 
3. Describe one real-life application in which *regression* combined with *prediction* may be useful. Describe the response and predictors. 
4. Describe one real-life application in which *regression* combined with *inference* may be useful. Describe the response and predictors. 	

## Problem 3 [40\%]

Install and learn to use R (<https://www.r-project.org/>) or Python, read the labs in Chapter 2 of the textbook. We recommend that you use R Notebooks of RStudio to typeset homeworks. Jupyter is a comparable tool for Python. Use Python or another tool (like MATLAB or Julia) if you have some experience and you will not need help from the TA/instructor. Then:

1. Download the advertising dataset (`Advertising.csv`) from <http://www-bcf.usc.edu/~gareth/ISL/data.html> and load it into R/Python (use function `read.csv()` in R or Pandas in Python)
2. What are the minimum, maximum, and mean value of each feature? (in R use function `summary()` and or `range()`)
3. Produce a scatterplot matrix of all variables (in R use function `pairs()`)
4. Produce a histogram of TV advertising (in R use function `hist()`)

## Hints
1. An easy way to launch help for any function in R, such as `summary`, is to execute:  `> ?summary`
2. See <http://rmarkdown.rstudio.com/pdf_document_format.html> for how to generate a PDF from an R notebook in R-studio. You will also need to install \LaTeX which you can get from <https://www.latex-project.org/get/>
3. For more advanced (and prettier?) plotting capabilities, see the package `ggplot`: <http://ggplot2.tidyverse.org/> and <https://github.com/rstudio/cheatsheets/raw/master/data-visualization-2.1.pdf>
4. If you think you may struggle with R, consider signing up for MATH 759, a 1-credit online introduction to R.