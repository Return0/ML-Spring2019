#!/bin/python
"""
Generates all dates that fall between the provided range and are also the
specific day of the week. This can be useful for generating a class schedule
"""
from datetime import date, timedelta as td

# start date
d1 = date(2019, 1, 22)
# end date
d2 = date(2019, 5, 7)

delta = d2 - d1

# which days to generate
# Monday == 1 ... Sunday == 7

validdays = {1,3}

print("| Date   | Day | Slides                                                          | Reading       | Notebooks")
print("| ------ | --- | -----------------------------                                   |------------   | ---------")

for i in range(delta.days + 1):
    d = d1 + td(days=i)
    if d.isoweekday() in validdays:
        print(d.strftime('|%b %d | %a | | | '))
