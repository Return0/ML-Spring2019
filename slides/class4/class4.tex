\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{nicefrac}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
\renewcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Logistic Regression}
\subtitle{and Maximum Likelihood}
\author{Marek Petrik}
\date{Feb 05, 2018}


\begin{document}
	
	\begin{frame} \maketitle
	\end{frame}


	\begin{frame} \frametitle{So Far in ML}
	\begin{itemize}
		\item Regression vs Classification
		\vfill
		\item Linear regression
		\vfill
		\item Bias-variance decomposition
		\vfill
		\item Nonlinear feature construction
		\vfill
		\item Quantitative vs qualitative features
	\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Simple Linear Regression}
	\begin{itemize}
		\item We have only one feature
		\[ Y \approx \beta_0 + \beta_1 X \qquad Y = \beta_0 + \beta_1 X + \epsilon \]
		\item Example:
		\begin{center}\includegraphics[width=0.8\linewidth]{{../figs/class2/sales_tv_reg}.pdf}\end{center}				
		\[ \varname{sales} \approx \beta_0 + \beta_1 \times \varname{TV} \]				
	\end{itemize}
	\end{frame}	

	\begin{frame} \frametitle{Multiple Linear Regression}
		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.4}.pdf}\end{center}
	\end{frame}

	\begin{frame}\frametitle{Types of Function $f$}
	\begin{columns}
		\begin{column}{0.5\linewidth}
			\centering
			\textbf{Regression}: continuous target
			\[ f : \mathcal{X} \rightarrow \mathbb{R} \]
			\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.6}.pdf}\end{center}	
		\end{column}
		\begin{column}{0.5\linewidth}
			\centering
			\textbf{Classification}: discrete target
			\[ f : \mathcal{X} \rightarrow \{ 1,2,3,\ldots, k \}\]
			\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.13}.pdf}\end{center}	
		\end{column}		
	\end{columns}
	\end{frame}

	\begin{frame} \frametitle{Today}
		\begin{itemize}
			\item Why not use linear regression for classification
			\item Logistic regression 
			\item Maximum likelihood principle
			\item Maximum likelihood for linear regression
			\item Reading:
			\begin{itemize}
				\item ISL 4.1-3
				\item ESL 2.6 (max likelihood)
			\end{itemize}
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Examples of Classification}
		\begin{enumerate}[<+>]
			\item A person arrives at the emergency room with a set of symptoms
			that could possibly be attributed to one of three medical conditions.
			Which of the three conditions does the individual have?
			\item An online banking service must be able to determine whether or not
			a transaction being performed on the site is fraudulent, on the basis
			of the user’s IP address, past transaction history, and so forth.
			\item  On the basis of DNA sequence data for a number of patients with
			and without a given disease, a biologist would like to figure out which
			DNA mutations are deleterious (disease-causing) and which are not.
		\end{enumerate}
	\end{frame}

	\begin{frame} \frametitle{IBM Watson}
		\centering
		\includegraphics[width=0.8\linewidth]{../figs/class1/Watson_Jeopardy.jpg}
		{\tiny Fair use, https://en.wikipedia.org/w/index.php?curid=31142331} \\
		\vfill
		\textbf{Logistic regression} + clever function engineering
	\end{frame}

	\begin{frame} \frametitle{Predicting Default}
		\[ \varname{default} \approx f(\varname{income}, \varname{balance}) \]
		\only<1>{\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter4/4.1a}.pdf}\end{center}}
		\only<2>{\begin{center}Boxplot\\\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter4/4.1b}.pdf}\end{center}}
	\end{frame}

	\begin{frame} \frametitle{Casting Classification as Regression}
		\begin{itemize}
			\item \textbf{Regression}: $f: X \rightarrow \mathbb{R}$
			\item \textbf{Classification}: $f: X \rightarrow \{ 1,2,3 \}$
			\vfill
			\item<2-> But $\{1,2,3\} \subseteq \mathbb{R}$
			\item<2-> Do we even need classification?
			\vfill
			\item<3-> \textbf{Yes!}
			\item<3-> \textbf{Regression}: Values that are close are similar
			\item<3-> \textbf{Classification}: Distance of classes is meaningless
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Casting Classification as Regression: Example}
		\begin{itemize}
			\item Predict possible diagnosis: 
				\[\{ \varname{stroke}, \varname{overdose}, \varname{seizure} \}\]
			\item Assign class labels:
			\[ Y = \begin{cases}
				1 &\text{if } \varname{stroke} \\
				2 &\text{if } \varname{overdose} \\
				3 &\text{if } \varname{seizure}
			\end{cases}~. \]
			\item Fit linear regression
			\item<2-> \textbf{Make predictions}: If uncertain whether symptoms point to $\varname{stroke}$ or $\varname{seizure}$, we predict $\varname{overdose}$
		\end{itemize}	
	\end{frame}

	\begin{frame} \frametitle{Linear Regression for 2-class Classification}
	\[ Y =  \begin{cases}
	1 &\text{if } \varname{default} \\
	0 & \text{otherwise}
	\end{cases} \]
	\vfill
	\begin{center}
		Linear regression \hspace{20mm} Logistic regression\\
		\vspace{-7mm}
		\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.2}.pdf}
	\end{center}
	\[ \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] \]
	\end{frame}

	\begin{frame} \frametitle{Logistic Regression}
		\begin{itemize}
			\item Predict \textbf{probability} of a class: $p(X)$
			\item Example: $p(\varname{balance})$ probability of default for person with $\varname{balance}$
			\item \textbf{Linear regression}:
			\[ p(X) = \beta_0 + \beta_1  \]
			\item \textbf{logistic regression}:
			\[ p(X) = \frac{e^{\beta_0+\beta_1 \, X}}{1+ e^{\beta_0 + \beta_1\,X}}  \]
			\item the same as:
			\[ \log\left( \frac{p(X)}{1-p(X)}\right) = \beta_0 + \beta_1\,X \]
			\item \underline{Odds}: $\nicefrac{p(X)}{1-p(X)}$
		\end{itemize}
	\end{frame}


	\begin{frame} \frametitle{Logistic Function}
		\[ y = \frac{e^{x}}{1+ e^{x}} \]
		\begin{center}
			\vspace{-8mm}		
			\includegraphics[width=0.9\linewidth]{../figs/class4/logistic.pdf}
			\vspace{-5mm}		
		\end{center}
		\[ p(X) = \frac{e^{\beta_0+\beta_1 \, X}}{1+ e^{\beta_0 + \beta_1\,X}}  \]
	\end{frame}
	
	\begin{frame} \frametitle{Logit Function}
		\[ \log\left( \frac{p(X)}{1-p(X)}\right)  \]
		\begin{center}
			\vspace{-8mm}
			\includegraphics[width=0.9\linewidth]{../figs/class4/logit.pdf}
			\vspace{-5mm}
		\end{center}		
		\[ \log\left( \frac{p(X)}{1-p(X)}\right) = \beta_0 + \beta_1\,X \]
	\end{frame}

	\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
		\begin{itemize}
			\item \textbf{Likelihood}: Probability that data is generated from a model
			\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
			\item Find the most likely model:
			\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
			\item Likelihood function is difficult to maximize
			\item Transform it using $\log$ (strictly increasing)
			\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]
			\item Strictly increasing transformation does not change maximum
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Example: Maximum Likelihood}
		\begin{itemize}
			\item Assume a coin with $p$ as the probability of \emph{heads}
			\item \textbf{Data}: \underline{$h$ heads, $t$ tails}
			\item The likelihood function is:
			\[ \ell(p) = {h+t \choose h} p^h \, (1-p)^t \approx p^h \, (1-p)^t~.\]
			\begin{center}				
				\vspace{-7mm}
				\includegraphics[width=\linewidth]{../figs/class4/likelihood10.pdf}
			\end{center} 
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Likelihood Function: 2 coin flips}
			\begin{center}				
			\textbf{heads} $h=1$ \qquad \textbf{tails} $t=1$\\
			\vspace{-5mm}
			\includegraphics[width=\linewidth]{../figs/class4/likelihood1.pdf}
			\vspace{-5mm}
			\end{center} 
	\end{frame}

	\begin{frame} \frametitle{Likelihood Function: 20 coin flips}
	\begin{center}				
		\textbf{heads} $h=10$ \qquad \textbf{tails} $t=10$\\
		\vspace{-5mm}
		\includegraphics[width=\linewidth]{../figs/class4/likelihood10.pdf}
		\vspace{-5mm}
	\end{center} 
	\end{frame}

	\begin{frame} \frametitle{Likelihood Function: 200 coin flips}
	\begin{center}				
		\textbf{heads} $h=100$ \qquad \textbf{tails} $t=100$\\
		\vspace{-5mm}
		\includegraphics[width=\linewidth]{../figs/class4/likelihood100.pdf}
		\vspace{-5mm}
	\end{center} 
	\end{frame}

	\begin{frame} \frametitle{Maximizing Likelihood}
		\begin{itemize}
			\item Likelihood function is not concave: hard to maximize
			\[ \ell(p) = p^h \, (1-p)^t ~.\]
			\item Maximize the log-likelihood instead
			\[ \log \ell(p) = h\,\log(p) + t\, \log(1-p) ~.\]
			\begin{center}				
				\vspace{-9mm}
				\includegraphics[width=\linewidth]{../figs/class4/loglikelihood.pdf}
			\end{center} 
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Log-likelihood: Biased Coin}
		\begin{center}
			\textbf{heads} $h=20$ \qquad \textbf{tails} $t=50$\\				
			\vspace{-5mm}
			\includegraphics[width=\linewidth]{../figs/class4/loglikelihood_biased.pdf}
		\end{center} 
	\end{frame}

	\begin{frame} \frametitle{Maximize Log-likelihood}
		\begin{itemize}
			\item Log-likelihood:
			\[ \log \ell(p) = h\,\log(p) + t\, \log(1-p) ~.\]
			\vfill
			\item<2-> Maximum where derivative = 0
			\item<2-> Derivative:
			\[ \frac{d}{dp} h\,\log(p) + t\, \log(1-p) = \frac{h}{p} - \frac{t}{1-p} \] 
			\vfill
			\item<3-> Maximum likelihood solution:
			\[ p = \frac{h}{h+t} \]
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Max-likelihood: Logistic Regression}
		\begin{itemize}
			\item Features $x_i$ and labels $y_i$
			\item Likelihood:
			\[ \ell(\beta_0,\beta_1)  = \prod_{i : y_i =1} p(x_i) \prod_{i:y_i=0} (1-p(x_i)) \]
			\item Log-likelihood:
			\[ \ell(\beta_0,\beta_1) = \sum_{i: y_i =1} \log p(x_i) + \sum_{i:y_i=0} \log (1-p(x_i)) \]
			\item Concave maximization problem
			\item Can be solved using gradient descent			
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Multiple Logistic Regression}
		\begin{itemize}
			\item Multiple features
			\[ p(X) = \frac{e^{\beta_0 + \beta_1 X_1 + \beta_2 X_2 + \ldots + \beta_m X_n}}{1 + e^{\beta_0 + \beta_1 X_1 + \beta_2 X_2 + \ldots + \beta_m X_n}}   \]
			\item Equivalent to:
			\[ \log \left( \frac{p(X)}{1-p(X)} \right) = \beta_0 + \beta_1 X_1 + \beta_2 X_2 + \ldots + \beta_m X_n \]
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Multinomial Logistic Regression}
		\begin{itemize}
			\item Predicting multiple classes:
			\begin{itemize}
				\item Medical diagnosis
				\[ Y = \begin{cases}
					1 &\text{if } \varname{stroke} \\
					2 &\text{if } \varname{overdose} \\
					3 &\text{if } \varname{seizure}
				\end{cases}~. \]
				\item Predicting which products customer purchases
			\end{itemize}
			\item Straightforward generalization of simple logistic regression
			\[ \frac{e^{c_1}} {1+e^{c_1}} \quad \Rightarrow \quad \frac{e^{c_1}} {e^{c_1}+e^{c_2}+\ldots+e^{c_k}} \]
		\end{itemize}
	\end{frame}

\end{document}