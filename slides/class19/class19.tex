\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}

\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
\renewcommand{\P}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Probabilistic Machine Learning}
\subtitle{Graphical Models and Bayes Nets}
\author{Marek Petrik}
\date{April 16 2018}


\begin{document}
	
\begin{frame} 
\maketitle	

\begin{center}
\textbf{Based on}: Stuart J. Russell and Peter Norvig (2010). Artificial Intelligence A Modern Approach, 3rd edition. \\
\emph{See also}: P. Murphy, K. (2012). Machine Learning: A Probabilistic Perspective. Chapter~10.
\end{center}
\end{frame}

\begin{frame} \frametitle{Estimating Probability Distribution}

\begin{itemize}
	\item Probability that a hurricane hits?
	\item Probability of stock market crash? 
	\item Probability of a species dying out?
	\pause
	\vfill
	\item Can we do that already?
	\pause
	\vfill
	\item Hurricane uncertainty cone
	\item Distribution of stock returns
	\item Distribution of a species
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Hurricane Uncertainty Cone}
\centering
\includegraphics[width=0.7\linewidth]{../figs/class19/hurricane.png}
\end{frame}

\begin{frame} \frametitle{Probability Distributions}
\begin{itemize}
	\item \textbf{Discrete random variable}:
	\begin{itemize}
		\item Qualitative values
		\item \emph{Distributions}: Bernoulli, multinomial
	\end{itemize}
	\vfill
	\item \textbf{Continuous random variable}:
	\begin{itemize}
		\item Qualitative values
		\item \emph{Distributions}: Normal, Poisson, Beta, Dirichlet, Cauchy, \ldots
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Distribution of Cars: Continuous or Discrete}
\begin{itemize}
	\item mpg 
	\item cylinders    
	\item displacement
	\item horsepower
	\item weight
	\item acceleration
	\item origin
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Probability Distribution}
\begin{itemize}
	\item Burglar alarm installed
	\item May also respond to earthquakes
	\item \emph{John} and \emph{Mary} are neighbors
	\item John always hears the alarm, but sometimes a phone
	\item Mary sometimes does not hear the alarm
\end{itemize}
\end{frame}

\newcommand{\sB}{\mathtt{burglar}}
\newcommand{\sE}{\mathtt{quake}}
\newcommand{\sA}{\mathtt{alarm}}
\newcommand{\sJ}{\mathtt{john}}
\newcommand{\sM}{\mathtt{mary}}

\begin{frame} \frametitle{Random Variables}
\begin{enumerate}
	\item Burglar: Yes, No
	\item Earthquake: Yes, No
	\item Alarm: Yes, No
	\item John calls: Yes, No
	\item Mary calls: Yes, No
\end{enumerate}

\[ \P[\sB, \sE, \sA, \sJ, \sM] = ? \]
\end{frame}

\begin{frame} \frametitle{Representing Probability Distribution}
\[ \P[\sB, \sE, \sA, \sJ, \sM] = ? \]
\vfill
\begin{center}
\begin{tabular}{ccccc|r}
	Burglar & Earthquake & Alarm & John & Mary & $\P$ \\
	\hline
	Yes & Yes & Yes & Yes & Yes & 0.01 \\
	Yes & Yes & Yes & Yes & No & 0.2 \\
	\ldots & & & & & \ldots \\
	No & No & No & No & No & 0.01 
\end{tabular}
\end{center}
\pause
\vfill
\textbf{Grows exponentially with number of variables}
\end{frame}

\begin{frame} \frametitle{Conditional Independence}
\begin{itemize}
	\item Independent random variables
	\[ \P[X,Y] = \P[X] \P[Y] \]
	\item Convenient, but not true often enough
	\pause
	\item \textbf{Conditional} independence
	\[ X \bot Y | Z \Leftrightarrow \P[X,Y | Z] = \P[X | Z] \P[Y|Z] \]
	\item Use conditional independence in machine learning
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Dependent but Conditionally Independent}
Events with a possibly biased coin:
\begin{enumerate}
	\item $X$: Your first coin flip is heads
	\item $Y$: Your second flip is heads
	\item $Z$: Coin is biased
\end{enumerate}
\pause
\vfill
\begin{itemize}
	\item $X$ and $Y$ are \underline{not independent}
	\item $X$ and $Y$ are \underline{independent} given $Z$
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Independent but Conditionally Dependent}
Is this possible? \pause \textbf{Yes!}
Events with an unbiased coin:
\begin{enumerate}
\item $X$: Your first coin flip is heads
\item $Y$: Your second flip is heads
\item $Z$: The coin flips are the same
\end{enumerate}
\pause
\vfill
\begin{itemize}
\item $X$ and $Y$ are \underline{independent}
\item $X$ and $Y$ are \underline{not independent} given $Z$
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Bayes Nets}
\begin{center}
	 Graphical representation of conditional independence \\[1cm]
	 \includegraphics[width=0.7\linewidth]{../figs/class19/directed_alarm.png}
\end{center}
\end{frame}

\begin{frame} \frametitle{Conditional Probability Table}
\begin{itemize}
	\item Burglary: $\P[\sB] = 0.001 $
	\item Earthquake: $\P[\sE] = 0.002$
	\item John: \\
	\begin{tabular}{c|c}
		$\sA$ & $\P[\sJ]$ \\
		\hline
		Yes & 0.9 \\
		No & 0.05
	\end{tabular}
	\item Mary: \\
	\begin{tabular}{c|c}
		$\sA$ & $\P[\sM]$ \\
		\hline
		Yes & 0.7 \\
		No & 0.01
	\end{tabular}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Joint Probability Distribution}
\begin{gather*}
\P[\sB, \sE, \sA, \sJ, \sM] = \\ 
\P[\sB] \cdot \P[\sE] \cdot \P[\sA | \sB, \sE] \cdot \\
\P[\sJ | \sA] \cdot \P[\sM | \sA] 
\end{gather*}
\pause
\begin{gather*}
\P[\sB, \neg\sE, \sA, \sJ, \sM] = \\ 
\P[\sB] \cdot \P[\neg\sE] \cdot \P[\sA | \sB, \neg\sE] \cdot \\
\P[\sJ | \sA] \cdot \P[\sM | \sA] 
\end{gather*}
\end{frame}


\begin{frame} \frametitle{Directed Graphical Models}
\begin{itemize}
	\item Represent complex structure of conditional independence
	\pause
	\item Node is independent of all predecessors \textbf{conditional} on parent value
	\[ x_s \; \bot \; x_{pred(s) \setminus pa(s)} \; | \; x_{pa(s)}  \]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{../figs/class19/directed.png}
	\end{center}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Inference: Answering Questions}
\begin{itemize}
	\item Mary called, what is the probability of an earthquake?
	\vfill
	\item John called, what is the probability of a burglary?
	\vfill
	\item If there is an earthquake, what is the probability I find out?
	\vfill
	\item The alarm rings, is it earthquake or burglary?
\end{itemize}
\end{frame}


\begin{frame} \frametitle{Inference Formalized}
\begin{itemize}
	\item Mary called, what is the probability of an earthquake?
	\[ P[\sE | \sM, \neg \sJ] = \frac{P[\sE, \sM, \neg \sJ]}{\P[\sM, \neg \sJ]} \]
	\vfill
	\item If there is an earthquake, what is the probability Mary calls?
	\[ P[\sM | \sE] = \frac{P[\sM, \sE]}{\P[\sE]} \]
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Variables}
\begin{enumerate}
	\item \textbf{Observable}: what is known
	\item \textbf{Query}: what is sought
	\item \textbf{Hidden} or latent: unknown
\end{enumerate}
What is hidden and observable?
\begin{itemize}
	\item Mary called, what is the probability of an earthquake?
	\item If there is an earthquake, what is the probability John calls?
	\item The alarm rings, is it earthquake or burglary?
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Marginalization}
\[ \P[A=a] = \sum_b \P[A=a,B=b] \]
\vfill
\begin{center}
	\begin{tabular}{c|cc}
		& Rain 	& Sunny \\
		\hline
		Sox win 	& 0.1	& 0.5 	\\
		Sox lose 	& 0.3	& 0.1 	
	\end{tabular}
\end{center}
\pause
\begin{itemize}
	\item Probability Red Sox win?
	\item Probability it will be sunny?
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Marginalization Example}
Mary does not know my office number (cannot call):
\begin{gather*} 
\P[\sB, \sE, \sA, \sJ] = \\\P[\sB, \sE, \sA, \sJ, \sM] + \\ +
\P[\sB, \sE, \sA, \sJ, \neg\sM]
\end{gather*}
\end{frame}

\begin{frame} \frametitle{Efficient Marginalization}
Worst-case complexity of marginalization is \textbf{exponential}.\\
Methods:
\begin{enumerate}
	\item \textbf{Exact methods}: Variable eliminations
	\item \textbf{Approximate methods}: Variational inference
	\item \textbf{Sampling methods}: 
	\begin{enumerate}
		\item Direct sampling
		\item Rejection sampling
		\item Markov Chain Monte Carlo: Gibbs and Metropolis-Hastings sampling
	\end{enumerate}
\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Markov Blanket}
\centering
\includegraphics[width=0.8\linewidth]{../figs/class19/blanket.png}
\end{frame}

\begin{frame} \frametitle{Learning in Bayes Nets}
\begin{itemize}
	\item \textbf{Known structure and fully observable data}: Easy
	\vfill
	\item \textbf{Known structure hidden variables}: 
	\begin{enumerate}
		\item Expectation maximization (maximum likelihood)
		\item Inference! (Bayesian, Maximum A Posteriori)
	\end{enumerate}
	\vfill
	\item \textbf{Unknown structure}: Very hard
\end{itemize}
See  P. Murphy, K. (2012). Machine Learning: A Probabilistic Perspective. Chapter~10.
\end{frame}

\begin{frame} \frametitle{Learning Structure is Hard}
Needs a lot of data, non-convex optimization problem. Structure is not unique!
\begin{center}
	\includegraphics[width=\linewidth]{../figs/class19/structures.png}
\end{center}
\end{frame}

\begin{frame} \frametitle{Recall Alarm Structure}
\begin{center}
	\includegraphics[width=0.7\linewidth]{../figs/class19/directed_alarm.png}
\end{center}
Assume all is known except for $\P[\sE]$
\end{frame}

\begin{frame} \frametitle{Learning as Inference}
\begin{itemize}
	\item Example dataset: \\
	\begin{center}
		\begin{tabular}{cc}
			Burglary & Alarm \\
			\hline
			No & No \\
			Yes & Yes \\
			No  & Yes \\
			\ldots & \ldots
		\end{tabular}
	\end{center}
	\item Want to learn $\P[\sE]$
	\item Approach:
	\begin{enumerate}
		\item Replace the net for each data point
		\item Introduce a prior parameter $\beta$
		\item Inference: \textbf{query}: $\beta$, \textbf{hidden}: $\sE_i$, and \textbf{observable}: $\sB_i$ and $\sA_i$
	\end{enumerate}
\end{itemize}

\end{frame}

\begin{frame} \frametitle{Conditional Independence in Machine Learning}
\begin{itemize}
	\item Linear regression: $Y_i = \beta_0 + \beta_1 X_i + \epsilon_i$
	\pause
	\begin{itemize}
		\item $\epsilon_1$ and $\epsilon_2$ are independent
	\end{itemize}
	\vfill
	\pause
	\item LDA: $X \sim \mathcal{N}(\mu_y, \Sigma)$
	\pause
	\begin{itemize}
		\item $X_1$ and $X_2$ are independent given $Y_1$ and $Y_1$
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Conditional Independence and Learning}
\pause
\begin{itemize}
	\item Reduces number of parameters
	\pause
	\vfill
	\item Reduces bias or variance?
\end{itemize}
\end{frame}


\begin{frame} \frametitle{Bayes Nets and Related Models}
\begin{itemize}
	\item Naive Bayes 
	\vfill
	\item Markov Chains
	\vfill
	\item Hidden Markov Chains, Kalman Filter
	\vfill
	\item Undirected Graphical Models, Markov Random Fields
	\vfill
	\item Gaussian Processes
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Naive Bayes Model}
Closely related to QDA and LDA
\begin{center}
	\includegraphics[width=0.8\linewidth]{../figs/class19/naive_bayes.png}
\end{center}
\end{frame}


\begin{frame} \frametitle{Naive Bayes Model}
\begin{center}
	\includegraphics[width=0.5\linewidth]{../figs/class19/naive_bayes.png}
\end{center}
\begin{itemize}
	\item Chain rule
	\[ \P[x_1, x_2, x_3] = \P[x_1] \P[x_2 | x_1] \P[x_3 | x_1, x_2] \]
	\item Probability
	\[ \P[x, y] = \P[y] \prod_{j=1}^D \P[x_j | y] \]
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Markov Chain}

\begin{itemize}
	\item 1st order Markov chain:
	\begin{center}
		\includegraphics[width=0.8\linewidth]{../figs/class19/markov1.png}
	\end{center}
	\vfill
	\item 2nd order Markov chain:
	\begin{center}
		\includegraphics[width=0.8\linewidth]{../figs/class19/markov2.png}
	\end{center}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Uses of Markov Chains}
\begin{itemize}
		\item Time series prediction
		\item Simulation of stochastic systems
		\item Inference in Bayesian nets and models
		\item Many others \ldots
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Hidden Markov Models}

\begin{center}
	\includegraphics[width=0.8\linewidth]{../figs/class19/hmm.png}
\end{center}

Used for:
\begin{itemize}	
	\item Speech and language recognition
	\item Time series prediction 
	\item \textbf{Kalman filter}: version with normal distributions used in GPS's
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Undirected Graphical Models}
\begin{itemize}
	\item Another (different) representation of conditional independence
	\begin{center}
		\includegraphics[width=0.5\linewidth]{../figs/class19/undirected.png}
	\end{center}
	\item Markov Random Fields
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Gaussian Processes}
Also known as Kriging. Kernelized Bayesian linear regression, or an infinite-dimensional Gaussian distribution with a Gaussian prior (kernel).
\begin{center}
	\includegraphics[width=0.8\linewidth]{../figs/class19/chi_feng_gp_regression.png}
	{\tiny Source:\url{https://livingthing.danmackinlay.name/gaussian_processes.html}}
\end{center}
\end{frame}

\begin{frame} \frametitle{Probabilistic Modeling Languages}

\begin{itemize}
	\item Powerful frameworks to describe a Bayesian model
	\vfill
	\item Popular frameworks:
	\begin{itemize}
		\item JAGS
		\item BUGS, WinBUGS, OpenBUGS
		\item Stan
	\end{itemize}	
	\vfill
	\item Built-in inference algorithms
\end{itemize}

\end{frame}

\end{document}	
