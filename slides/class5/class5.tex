\documentclass{beamer}


\let\val\undefined

\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{nicefrac}
\usepackage{libertine}
\usepackage{multirow}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{LDA, QDA, Naive Bayes}
\subtitle{Generative Classification Models}
\author{Marek Petrik}
\date{February 12th, 2018}

\newcommand{\plus}{\textcolor{green}{$\mathbf{+}$}}
\newcommand{\minus}{\textcolor{red}{$\mathbf{-}$}}

\begin{document}
	
\begin{frame} \maketitle
\end{frame}

\begin{frame}\frametitle{Last Class}
	\begin{itemize}
		\item Logistic Regression
		\item Maximum Likelihood Principle
	\end{itemize}	
\end{frame}

	\begin{frame} \frametitle{Logistic Regression}
	\begin{itemize}
		\item Predict \textbf{probability} of a class: $p(X)$
		\item Example: $p(\varname{balance})$ probability of default for person with $\varname{balance}$
		\item \textbf{Linear regression}:
		\[ p(X) = \beta_0 + \beta_1\, X  \]
		\item \textbf{Logistic regression}:
		\[ p(X) = \frac{e^{\beta_0+\beta_1 \, X}}{1+ e^{\beta_0 + \beta_1\,X}}  \]		
		\item Linear mode of log-odds:
		\[ \log\left( \frac{p(X)}{1-p(X)}\right) = \beta_0 + \beta_1\,X \]
		\item \underline{Odds}: $\nicefrac{p(X)}{1-p(X)}$
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Logistic Function}
	\[ y = \frac{e^{x}}{1+ e^{x}} \]
	\begin{center}
		\vspace{-8mm}		
		\includegraphics[width=0.9\linewidth]{../figs/class4/logistic.pdf}
		\vspace{-5mm}		
	\end{center}
	\[ p(X) = \frac{e^{\beta_0+\beta_1 \, X}}{1+ e^{\beta_0 + \beta_1\,X}}  \]
\end{frame}

\begin{frame} \frametitle{Logit Function}
	\[ \log\left( \frac{p(X)}{1-p(X)}\right)  \]
	\begin{center}
		\vspace{-8mm}
		\includegraphics[width=0.9\linewidth]{../figs/class4/logit.pdf}
		\vspace{-5mm}
	\end{center}		
	\[ \log\left( \frac{p(X)}{1-p(X)}\right) = \beta_0 + \beta_1\,X \]
\end{frame}

\begin{frame} \frametitle{Logistic Regression}
	\[ \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}]  = \frac{e^{\beta_0 + \beta_1 \varname{balance} } }{ 1+ e^{\beta_0 + \beta_1 \varname{balance} } } \]
	\vfill
	\begin{center}
		Linear regression \hspace{20mm} Logistic regression\\
		\vspace{-7mm}
		\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.2}.pdf}
	\end{center}
\end{frame}

\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
	\begin{itemize}
		\item \textbf{Likelihood}: Probability that data is generated from a model
		\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
		\item Find the most likely model:
		\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
		\item Likelihood function is difficult to maximize
		\item Transform it using $\log$ (strictly increasing)
		\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]
		\item Strictly increasing transformation does not change maximum
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Example: Maximum Likelihood}
	\begin{itemize}
		\item Assume a coin with $p$ as the probability of \emph{heads}
		\item \textbf{Data}: \underline{$h$ heads, $t$ tails}
		\item The likelihood function is:
		\[ \ell(p) = p^h \, (1-p)^t ~.\]
		\begin{center}				
			\vspace{-7mm}
			\includegraphics[width=\linewidth]{../figs/class4/likelihood10.pdf}
		\end{center} 
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Maximizing Likelihood}
	\begin{itemize}
		\item Likelihood function is not concave: hard to maximize
		\[ \ell(p) = p^h \, (1-p)^t ~.\]
		\item Maximize the log-likelihood instead
		\[ \log \ell(p) = h\,\log(p) + t\, \log(1-p) ~.\]
		\begin{center}				
			\vspace{-9mm}
			\includegraphics[width=\linewidth]{../figs/class4/loglikelihood.pdf}
		\end{center} 
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Machine Learning Choices ...}
\centering
\includegraphics[width=\linewidth]{../figs/class1/ml_map.png}\\
{\tiny Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/index.html}} \\[3mm]
\end{frame}

\begin{frame}\frametitle{Today}
	\begin{enumerate}
		\item Classification methods continued 
		\vfill
		\item Discriminative vs. Generative ML Models
		\vfill
		\item Generative classification models:
		\begin{itemize}
			\item Linear Discriminant Analysis (LDA)
			\item Quadratic Discriminant Analysis (QDA)
		\end{itemize}
		\vfill
		\item Confusion matrix
	\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Discriminative vs Generative Models}
	\begin{itemize}
		\item \textbf{Discriminative models}
		\begin{itemize}
			\item Estimate conditional models $\Pr[Y \mid X]$ 
			\item Linear regression 
			\item Logistic regression
		\end{itemize}
		\vfill
		\item \textbf{Generative models}
		\begin{itemize}
			\item Estimate joint probability $\Pr[Y, X] = \Pr[Y \mid X] \Pr[X]$
			\item Estimates not only probability of labels but also the features
			\item Once model is fit, can be used to generate data
			\item LDA, QDA, Naive Bayes
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Discriminative Machine Learning}
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
	\node[block](data){Dataset};
	\node[block,below of=data](algo){ML Algorithm};
	\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
	\node[left of=hypo,xshift=-1cm](input) {Features $\tcg{X}$};
	\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
	\path (data) edge (algo) 
	(algo) edge (hypo)
	(input) edge [dashed] (hypo)
	(hypo) edge [dashed] (output);
	\end{tikzpicture}
\end{frame}

\begin{frame} \frametitle{Generative Machine Learning}
\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
	\node[block](data){Dataset};
	\node[block,below of=data](algo){ML Algorithm};
	\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
	\node[left of=hypo,xshift=-1cm](input) {Features $\tcg{X}$};
	\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
	\path (data) edge (algo) 
	(algo) edge (hypo)
	(hypo) edge [dashed] (input)
	(output) edge [dashed] (hypo);
	\visible<2->{
		\node[block,right of=algo,xshift=1cm](prior){Class prior $\tcr{\pi}$};
		\path (algo) edge (prior) 
			  (prior) edge [dashed] (output);	
	}
	\end{tikzpicture}
\end{frame}

\begin{frame}\frametitle{Generative Models}
	\begin{itemize}
		\item[\plus] Can be used to generate data ($\Pr[X]$)
		\item[\plus] Offers more insights into data
		\item[\minus] Often works worse, particularly when assumptions are violated
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Normal Distribution}
	\begin{itemize}
		\item Density function:
		\[ p(x) = \frac{1}{\sigma\sqrt{2\pi}} e^{-\frac{(x-\mu)^2}{2\sigma^2}} \]
		\begin{center}
			\includegraphics[width=0.5\linewidth]{../figs/class5/normal.pdf}
		\end{center}
		\item<2-> \textbf{Central limit theorem}: $Z = \nicefrac{1}{n} \sum_{i=1}^n X_i$ for i.i.d. $X_i$ is normal with $n\rightarrow \infty$
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Logistic Regression}
	\[ Y =  \begin{cases}
	1 &\text{if } \varname{default} \\
	0 & \text{otherwise}
	\end{cases} \]
	\vfill
	\begin{center}
		Linear regression \hspace{20mm} Logistic regression\\
		\vspace{-7mm}
		\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.2}.pdf}
	\end{center}
	Predict:
	\[ \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] \]
\end{frame}

\begin{frame}\frametitle{LDA: Linear Discriminant Analysis}
	\begin{itemize}
		\item \textbf{Generative model}: capture probability of predictors for each label		
    	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.4}.pdf}\end{center}	
		\item Predict:
		\begin{enumerate}
			\item<2-> $\Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ]$ and $\Pr[\varname{default} = \operatorname{yes}]$
			\item<3-> $\Pr[\varname{balance} \mid \varname{default} = \operatorname{no} ]$ and $\Pr[\varname{default} = \operatorname{no}]$
		\end{enumerate}
		\item<4-> Classes are normal: $\Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ]$
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{LDA vs Logistic Regression}
	\begin{itemize}
		\item \textbf{Logistic regressions}:
			\[ \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] \]
		\vfill
		\item \textbf{Linear discriminant analysis}:
			\[ \Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ] \text{ and } \Pr[\varname{default} = \operatorname{yes}] \]
			\[ \Pr[\varname{balance} \mid \varname{default} = \operatorname{no} ] \text{ and } \Pr[\varname{default} = \operatorname{no}] \]
	\end{itemize}	
\end{frame}

\begin{frame}\frametitle{LDA with 1 Feature}
	\begin{itemize}
		\item Classes are normal and class probabilities $\pi_k$ are scalars
		\[ f_k(x) = \frac{1}{\sigma\sqrt{2\pi}} e^{  - \frac{1}{2\sigma^2} (x-\mu_k)^2 } \]
		\item \textbf{Key Assumption}: Class variances $\sigma_k^2$	\underline{are the same}, means are the same.
		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.4}.pdf}\end{center}	
	\end{itemize}
	
\end{frame}

\begin{frame}\frametitle{Bayes Theorem}
	\begin{itemize}
		\item Classification from label distributions:
			\[ \Pr[Y = k \mid X = x] = \frac{\Pr[X = x \mid Y = k] \Pr[Y = k]}{\Pr[X = x]} \]
		\item<2-> Example:
			\begin{gather*} \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance} = \$100] = \\ \frac{\Pr[\varname{balance} = \$100 \mid \varname{default} = \operatorname{yes}] \Pr[\varname{default} = \operatorname{yes}]}{\Pr[\varname{balance} = \$100]} 
			\end{gather*}
		\item<3-> Notation:
			\[ \Pr[Y = k \mid X = x] = \frac{f_k(x) \pi_k}{\sum_{l=1}^{K} \pi_l f_l(x)} \]
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Classification With LDA}
	$X: \varname{balance}$, $Y: \varname{default} \in \{ \operatorname{yes}, \operatorname{no}\}$
	\begin{align*}
	\text{Probability in class } \tcg{k_1} &> \text{Probability in class } \tcr{k_2} \\
	\visible<2->{\Pr[Y = \tcg{k_1} \mid X = x] &> \Pr[Y = \tcr{k_2} \mid X = x] \\}
	\visible<3->{\frac{\pi_{\tcg{k_1}} f_{\tcg{k_1}}(x)}{\sum_{l=1}^{K} \pi_l f_l(x)} &> \frac{\pi_{\tcr{k_2}} f_{\tcr{k_2}}(x)}{\sum_{l=1}^{K} \pi_l f_l(x)} \\}
	\visible<4->{\pi_{\tcg{k_1}} f_{\tcg{k_1}}(x) &> \pi_{\tcr{k_2}} f_{\tcr{k_2}}(x) \\} 
	\visible<5->{\log \left(\pi_{\tcg{k_1}} f_{\tcg{k_1}}(x) \right) &> \log\left(\pi_{\tcr{k_2}} f_{\tcr{k_2}}(x) \right) \\} 
	\visible<6->{\hat{\delta}_{\tcg{k_1}}(x) &>  \hat{\delta}_{\tcr{k_2}}(x) }	
	\end{align*}
	\visible<6->{
		\textbf{Discriminant function}: 
		\[\hat{\delta}_{\tcb{k}}(x) = x\cdot \frac{\hat{\mu}_{\tcb{k}}}{\hat{\sigma}^2} - \frac{\hat{\mu}_{\tcb{k}}^2}{2\hat{\sigma}^2} + \log(\hat{\pi}_{\tcb{k}}) \]
		\emph{Derive at home}
	}
\end{frame}

\begin{frame}\frametitle{Estimating LDA Parameters}
	\begin{itemize}
		\item<2-> Maximum log likelihood!
		\begin{gather*} 
		\max_{\mu,\sigma}\; \log \ell(\mu,\sigma) =  \max_{\mu,\sigma}\; \sum_{i=1}^N \log\left( f_{y_i}(x_i) \right) = \\
		\max_{\mu,\sigma}\; \sum_{i=1}^N \log\left( \frac{1}{\sigma\sqrt{2\pi}} \exp\left(  - \frac{1}{2\sigma^2} (x_i-\mu_{y_i})^2 \right) \right) = \\
		\max_{\mu,\sigma}\; \sum_{i=1}^N \left(-\log \sigma - \frac{1}{2\sigma^2} (x_i-\mu_{y_i})^2 + \operatorname{consts} \right)
		\end{gather*}
		\item<3-> Concave in $\mu$ and $\nicefrac{1}{\sigma^2}$, consider a single class with mean $\mu$
		\begin{align*}
		\frac{\partial}{\partial \mu} \log \ell(\mu,\sigma) &= \frac{1}{\sigma^2} \sum_{i=1}^N (x_i - \mu) = 0 \\
		\frac{\partial}{\partial \sigma} \log \ell(\mu,\sigma) &= \frac{N}{\sigma} + \frac{1}{\sigma^3} \sum_{i=1}^N (x_i - \mu)^2 = 0
		\end{align*}
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimating LDA Parameters}
\begin{itemize}
	\item $\log \ell$ is derivatives:
	\begin{align*}
	\frac{\partial}{\partial \mu} \log \ell(\mu,\sigma) &= \frac{1}{\sigma^2} \sum_{i=1}^N (x_i - \mu) = 0 \\
	\frac{\partial}{\partial \sigma} \log \ell(\mu,\sigma) &= \frac{N}{\sigma} + \frac{1}{\sigma^3} \sum_{i=1}^N (x_i - \mu)^2 = 0
	\end{align*}
	\item Therefore:
	\begin{align*}
	\mu  &= \frac{1}{N} \sum_{i=1}^N x_i \\
	\sigma^2 &= \frac{1}{N} \sum_{i=1}^N (x_i - \mu)^2 
	\end{align*}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Better Parameter Estimates}
	\begin{itemize}
		\item Maximum likelihood variance $\sigma^2$ is biased:
		\begin{align*}
		\mu  &= \frac{1}{N} \sum_{i=1}^N x_i \\
		\sigma^2 &= \frac{1}{N} \sum_{i=1}^N (x_i - \mu)^2 
		\end{align*}
		\item Unbiased estimate:
		\begin{align*}
		\mu  &= \frac{1}{N} \sum_{i=1}^N x_i \\
		\sigma^2 &= \frac{1}{N-1} \sum_{i=1}^N (x_i - \mu)^2 
		\end{align*}
		\item \emph{See ISL for precise formula for more than a single class}
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{LDA with Multiple Features}
	\begin{itemize}
		\item Multivariate Normal Distributions:
		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.5}.pdf}\end{center}
		\item Multivariate normal distribution density (mean vector $\mu$, covariance matrix $\Sigma$):
		\[ p(X) = \frac{1}{(2\pi)^{p/2} |\Sigma|^{\nicefrac{1}{2}}} \exp \left(-\frac{1}{2} (x-\mu)^\top\Sigma^{-1} (x-\mu) \right) \]
	\end{itemize}
\end{frame}

\newcommand{\Tr}{\operatorname{Trace}}

\begin{frame}\frametitle{Multivariate Maximum Likelihood}
	\begin{itemize}
	\item Consider a singe class:
	\begin{tiny}
		\begin{gather*} 
		\max_{\mu,\Sigma}\; \log \ell(\mu,\Sigma) =  \max_{\mu,\Sigma}\; \sum_{i=1}^N \log\left( f_k(x_i) \right) = \\
		\max_{\mu,\Sigma}\; \sum_{i=1}^N \log\left( \frac{1}{(2\pi)^{p/2} |\Sigma|^{\nicefrac{1}{2}}} \exp \left(-\frac{1}{2} (x_i-\mu)^\top\Sigma^{-1} (x_i-\mu) \right) \right) = \\
		\max_{\mu,\Sigma}\; - \frac{N}{2} \log |\Sigma| - \frac{1}{2} \sum_{i=1}^N  (x_i-\mu)^\top\Sigma^{-1} (x_i-\mu) = \\
		\max_{\mu,\Sigma}\; - \frac{N}{2} \log |\Sigma| - \frac{1}{2}  \Tr\left(  \Sigma^{-1} \sum_{i=1}^N   (x_i-\mu)^\top (x_i-\mu) \right)
		\end{gather*}
	\end{tiny}		
	\item Use $\nicefrac{\partial}{\partial \Sigma} \log |\Sigma| = \Sigma^{-\top}$ and $\nicefrac{1}{\partial A} \Tr(A B) = B^\top$
	\[ \Sigma = \frac{1}{N} \sum_{i=1}^N   (x_i-\mu)^\top (x_i-\mu) \]
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Multivariate Classification Using LDA}
	\begin{itemize}
		\item \textbf{Linear}: Decision boundaries are linear
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.6}.pdf}\end{center}	
\end{frame}

\begin{frame} \frametitle{Measuring Error in Classification}

	\begin{itemize}
		\item Mean squared error in regression:
		\[ \varname{MSE} = \frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{f}(x_i))^2  \]
		\item Classification error:
		\[ \varname{CE} = \frac{1}{n} \sum_{i=1}^{n} \mathbf{1}_{y_i \neq \hat{f}(x_i)}  \]
		\item Low classification error does not always mean ``good predictions''!
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Confusion Matrix: Predict $\varname{default}$}
	\begin{center}
	\begin{tabular}{l|l|c|c|c}
	\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{True}} & \\ 
	\cline{3-4}
	\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\ 
	\cline{2-4}
	\multirow{2}{*}{\textbf{Predicted}}
	& Yes & \tcg{$a$} & \tcr{$b$} & $a+b$\\   
	\cline{2-4}
	& No & \tcr{$c$} & \tcg{$d$} & $c+d$\\    
	\cline{2-4}
	\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$a+c$} & \multicolumn{    1}{c}{$b+d$} & \multicolumn{1}{c}{$N$}     
	\end{tabular}
	\end{center}
\textbf{LDA}: Predict default if $\Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] > \nicefrac{1}{2}$
	\begin{center}
	\begin{tabular}{l|l|c|c|c}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{True}} & \\ 
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\ 
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Yes & \tcg{$81$} & \tcr{$23$} & $104$\\   
		\cline{2-4}
		& No & \tcr{$252$} & \tcg{$9\,644$} & $9\,896$\\    
		\cline{2-4}
		\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$333$} & \multicolumn{    1}{c}{$9\,667$} & \multicolumn{1}{c}{$10\,000$}     
	\end{tabular}
	\end{center}
\begin{itemize}
	\item<2-> Good predictions?
	\item<3-> \textbf{No}: Most people who default are predicted as No default 
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Increasing LDA Sensitivity}
	\textbf{LDA}: Predict default if $\Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] > \nicefrac{1}{2}$
	\begin{center}
	\begin{tabular}{l|l|c|c|c}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{True}} & \\ 
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\ 
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Yes & \tcg{$81$} & \tcr{$23$} & $104$\\   
		\cline{2-4}
		& No & \tcr{$252$} & \tcg{$9\,644$} & $9\,896$\\    
		\cline{2-4}
		\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$333$} & \multicolumn{    1}{c}{$9\,667$} & \multicolumn{1}{c}{$10\,000$}     
	\end{tabular}
	\end{center}
\visible<2->{
	\textbf{Result of LDA classification}: Predict default if $\Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] > \nicefrac{1}{5}$
	\begin{center}
	\begin{tabular}{l|l|c|c|c}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{True}} & \\ 
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\ 
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Yes & \tcg{$195$} & \tcr{$235$} & $403$\\   
		\cline{2-4}
		& No & \tcr{$138$} & \tcg{$9\,432$} & $9\,570$\\    
		\cline{2-4}
		\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$333$} & \multicolumn{    1}{c}{$9\,667$} & \multicolumn{1}{c}{$10\,000$}     
	\end{tabular}
	\end{center}
}
\end{frame}

\begin{frame}\frametitle{True Positives, etc}
	\begin{center}
	\begin{tabular}{l|l|c|c|}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{Reality}}  \\ 
		\cline{3-4}
		\multicolumn{2}{c|}{} & Positive & Negative \\ 
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Positive & \tcg{True Positive} & \tcr{False Positive} \\   
		\cline{2-4}
		& Negative & \tcr{False Negative} & \tcg{True Negative} \\    
		\cline{2-4}
	\end{tabular}\end{center}
	\vspace{3mm}
	\begin{itemize}
		\item \textbf{Recall/sensitivity} = TP/(TP+FN)
		\item \textbf{Precision} = TP/(TP+FP)
		\item \textbf{Specificity} = TN/(TN+FP)
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{ROC Curve}
	\begin{center}
	\begin{tabular}{l|l|c|c|}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{Reality}}  \\ 
		\cline{3-4}
		\multicolumn{2}{c|}{} & Positive & Negative \\ 
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Positive & \tcg{True Positive} & \tcr{False Positive} \\   
		\cline{2-4}
		& Negative & \tcr{False Negative} & \tcg{True Negative} \\    
		\cline{2-4}
	\end{tabular}
	\end{center}
	\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter4/4.8}.pdf}\end{center}
\end{frame}

\begin{frame}\frametitle{Area Under ROC Curve}
	\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter4/4.8}.pdf}\end{center}	
	\begin{itemize}
		\item Larger area is better
		\item Many other ways to measure classifier performance, like $F_1$
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{QDA: Quadratic Discriminant Analysis}
	\begin{itemize}
		\item Generalizes LDA
		\vfill
		\item \textbf{LDA}: Class variances $\Sigma_k = \Sigma$ \underline{are the same}
		\item \textbf{QDA}: Class variances $\Sigma_k$ \underline{can differ}
		\vfill
		\item<2-> LDA or QDA has smaller training error on the same data?
		\item<3-> What about the test error?
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{QDA: Quadratic Discriminant Analysis}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.9}.pdf}\end{center}	
\end{frame}

\begin{frame}\frametitle{Naive Bayes}
	\begin{itemize}
		\item Yet another assumption on class-feature interactions, similar to QDA/LDA
		\begin{center}
			\includegraphics[width=0.5\linewidth]{../figs/class5/naivebayes.jpg}
		\end{center}
		\item With normal distribution over features $X_1, \ldots, X_k$ special case of QDA with \underline{diagonal} $\Sigma$
		\item Generalizes to non-Normal distributions and discrete variables
		\item More on this and other Bayesian models later \ldots
	\end{itemize}
\end{frame}

\end{document}