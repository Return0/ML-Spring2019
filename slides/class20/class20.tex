\documentclass[10pt]{beamer}

\usepackage{pgfpages}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{booktabs}
%\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}

\usepackage{adjustbox}
\usepackage{hyperref}

\usepackage{amsmath}

\usepackage{tikz}
\usetikzlibrary{positioning}

\usepackage{dutchcal}

%%% Set colors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% \setbeameroption{show notes on second screen=right} % Both
\setbeamertemplate{note page}{\pagecolor{yellow!5}\insertnote}\usepackage{palatino}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\pf}[1]{p'_{\bar{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Introduction to Deep Learning}

\date{Apr 23, 2018}
\author{Bence Cserna}


\begin{document}

\maketitle

\begin{frame}{Importance of Deep Learning}

\includegraphics[width=\textwidth]{../figs/class20/dl_perf}

\tiny
source: https://medium.com/@sayondutta/nuts-and-bolts-of-applying-deep-learning-by-andrew-ng 
\end{frame}

\begin{frame}{ImageNet Challenge}

Millions of hand labeled images with thousands of categories some of which include the bounding boxes.

\includegraphics[width=\textwidth]{../figs/class20/imgnet}

\tiny
source: Kaggle 
\end{frame}

\begin{frame}{ImageNet Performance}
\centering
\includegraphics[width=0.6\textwidth]{../figs/class20/imgnet_perf}

\end{frame}

\begin{frame}{Binary Classification}
\centering
\includegraphics[width=0.3\textwidth]{../figs/class20/cat}
\includegraphics[width=0.25\textwidth]{../figs/class20/hotdog}\\
\includegraphics[width=0.8\textwidth]{../figs/class20/MNIST}\\
28 x 28 images
\[
\begin{bmatrix}
    x_{1}     & \dots & x_{28} \\
   \vdots     & \ddots & x_{2n} \\
              & \dots & x_{m}
\end{bmatrix}
=
\begin{bmatrix}
    x_{1} & x_{2} & x_{3} & \dots  & x_{m} \\
\end{bmatrix}
\]
\end{frame}

\begin{frame}[t]{Notation}
\Large
$$ \beta_0  = \theta_0 =  b $$

$$ \beta_1  = \theta_1 = w_1 $$
$$ \beta_2  = \theta_2 = w_2 $$
$$ \vdots $$
$$ \beta_n  = \theta_n = w_n $$

\end{frame}

\begin{frame}[t]{Logistic Regression}
Linear regression:

\[
Y = \beta_0 + \beta_1 * x_1 + \beta_2 * x_2 \tag*{previously}
\]

\[
\hat{y} = w_1 * x_1 + w_2 * x_2 + b \tag*{DL notation}
\]

\[
\hat{y} = w^Tx + b \tag*{vectorized}
\]

Logistic regression:
\[
\sigma(z) = \frac{1}{1 + e^{-z}} \tag*{sigmoid function}
\]

$$\hat{y} = \sigma ( w^Tx + b ) $$\\
\end{frame}

\begin{frame}{Logistic Regressions as a Neural Network}

\def\layersep{2.5cm}
\centering
\begin{tikzpicture}[shorten >=1pt,->,draw=black!50, node distance=\layersep]
    \tikzstyle{every pin edge}=[<-,shorten <=1pt]
    \tikzstyle{neuron}=[draw, circle ,minimum size=18pt,inner sep=0pt]
    \tikzstyle{input neuron}=[neuron];
    \tikzstyle{output neuron}=[neuron];
    \tikzstyle{hidden neuron}=[neuron];
    \tikzstyle{annot} = [text width=5em, text centered]

    % Draw the input layer nodes
    \foreach \name / \y in {1,...,3}
    % This is the same as writing \foreach \name / \y in {1/1,2/2,3/3,4/4}
        \node[input neuron, pin=left:$x_\y$] (I-\name) at (0,-\y) {};

    % Draw the output layer node
    \node[output neuron,pin={[pin edge={->}]right:$\hat{y}$}, right of=I-2] (O) {};

    % Connect every node in the hidden layer with the output layer
    \foreach \source in {1,...,3}
        \path (I-\source) edge (O);

    % Annotate the layers
    \node[annot,above of=I-1, node distance=1cm] (hl) {Input  layer};
    \node[annot,right of=hl] {Output layer};
\end{tikzpicture}
\end{frame}

\begin{frame}{Loss and Cost Function}
Loss function: 

$$\mathbcal{L}(\hat{y}, y) = (y - \hat{y})^2 \triangleright \mathit{non-convex}$$

$$\mathbcal{L}(y, \hat{y}) = -(y\log(\hat{y}) + (1 - y)\log(1 - \hat{y}))$$

Cost function:

$$ \mathbcal{J} = \frac{1}{m} \sum_{i = 1}^{m} \mathbcal{L}(\hat{y}^i, y^i) $$

\end{frame}

\begin{frame}{Gradient Descent: Example}
\centering
\includegraphics[width=\textwidth]{../figs/class20/gradient_descent}

\tiny
source: https://medium.com/abdullah-al-imran/intuition-of-gradient-descent-for-machine-learning
\end{frame}


\begin{frame}{Deep Neural Networks}

\def\layersep{2.5cm}
\centering
\begin{tikzpicture}[shorten >=1pt,->,draw=black!50, node distance=\layersep]
    \tikzstyle{every pin edge}=[<-,shorten <=1pt]
    \tikzstyle{neuron}=[draw, circle ,minimum size=18pt,inner sep=0pt]
    \tikzstyle{input neuron}=[neuron];
    \tikzstyle{output neuron}=[neuron];
    \tikzstyle{hidden neuron}=[neuron];
    \tikzstyle{annot} = [text width=4em, text centered]

    % Draw the input layer nodes
    \foreach \name / \y in {1,...,4}
    % This is the same as writing \foreach \name / \y in {1/1,2/2,3/3,4/4}
        \node[input neuron, pin=left:$x_\y$] (I-\name) at (0,-\y) {};

    % Draw the hidden layer nodes
    \foreach \name / \y in {1,...,5}
        \path[yshift=0.5cm]
            node[hidden neuron] (H-\name) at (\layersep,-\y cm) {};

    % Draw the output layer node
    \node[output neuron,pin={[pin edge={->}]right:Output}, right of=H-3] (O) {};

    % Connect every node in the input layer with every node in the
    % hidden layer.
    \foreach \source in {1,...,4}
        \foreach \dest in {1,...,5}
            \path (I-\source) edge (H-\dest);

    % Connect every node in the hidden layer with the output layer
    \foreach \source in {1,...,5}
        \path (H-\source) edge (O);

    % Annotate the layers
    \node[annot,above of=H-1, node distance=1cm] (hl) {Hidden layer};
    \node[annot,left of=hl] {Input layer};
    \node[annot,right of=hl] {Output layer};
\end{tikzpicture}

\end{frame}

\begin{frame}{Activation Functions}

\centering

Sigmoid vs tanh :\\
\includegraphics[width=0.5\textwidth]{../figs/class20/sigmoid}\\
Rectified Linear Unit (ReLU):\\
\includegraphics[width=0.4\textwidth]{../figs/class20/relu}\\

\tiny
Source: http://ronny.rest, Kaggle
\end{frame}

\begin{frame}[standout]
  Deep Learning with R
  
\includegraphics[width=\textwidth]{../figs/class20/MnistExamples}\\
   
\end{frame}

\begin{frame} \frametitle{Learn More}

\begin{itemize}
	\item Keras in R: \url{https://keras.rstudio.com/}
	\item Deep learning book: \url{http://www.deeplearningbook.org/}
\end{itemize}
\end{frame}

\end{document}
