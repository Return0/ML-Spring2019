\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}

\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Bayesian Machine Learning}
\subtitle{MAP vs Max Likelihood}
\author{Marek Petrik}
\date{Feb 14, 2018}

\begin{document}

\begin{frame} \maketitle \end{frame}

\begin{frame} \frametitle{How Machine Learning Works}	
	\begin{itemize}
		\item Today's topics:
		\begin{itemize}
			\item Maximum likelihood
			\item Problems with maximum likelihood
			\item Bayesian methods
			\item Problems with Bayesian methods
		\end{itemize}
		\pause
		\item Machine Learning and How to Live		
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Algorithms to Live By}
\centering
\includegraphics[width=\linewidth]{../figs/class6/algotoliveby.png}
\end{frame}


\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
	\begin{itemize}
		\item \textbf{Likelihood}: Probability that data is generated from a model
		\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
		\item Find the most likely model:
		\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
		\item Likelihood function is difficult to maximize
		\item Transform it using $\log$ (strictly increasing)
		\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]
		\item Strictly increasing transformation does not change maximum
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Machine Learning so Far}
\begin{tabular}{l|l}
	\textbf{Method Name} & \textbf{Uses Max Likelihood} \\
	\hline \hline
	Linear Discriminant Analysis (LDA) & \pause\tcg{Yes} \\
	\pause Quadratic Discriminant Analysis (QDA) & \pause \tcg{Yes} \\	
	\pause Logistic Regression & \pause \tcg{Yes} \\
	\pause Least Squares, Linear Regression & \pause \tcg{Yes} \\
	\pause KNN & \pause \tcr{No}
\end{tabular}
\end{frame}


\begin{frame} \frametitle{Problems with Maximum Likelihood}
	\begin{enumerate}
		\item Why likelihood?
		\vfill
		\item Little data (one data point!??)
		\vfill
		\item Many degrees of freedom
		\vfill
		\item Some coefficient are more likely that others
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Learning from Small Data Sets}
	\emph{How many Wildcat buses are there?}
	\begin{center}
		\includegraphics[width=\linewidth]{../figs/class6/wildcat.jpg}
	\end{center}
	\pause
	\emph{A good guess is about 80.}
\end{frame}

\begin{frame} \frametitle{Number of Buses as Machine Learning}
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
\node[block](data){\textbf{Dataset}: Bus number $39$};
\node[block,below of=data](algo){ML Algorithm};
\node[block,below of=algo](hypo){\textbf{Hypothesis}: Bus count};
\node[left of=hypo,xshift=-1.3cm](input) {No input};
\node[right of=hypo,xshift=2.5cm](output) {Target:Bus number ($50$?)};
\path (data) edge (algo) 
(algo) edge (hypo)
(input) edge [dashed] (hypo)
(hypo) edge [dashed] (output);
\end{tikzpicture}
\end{frame}

\begin{frame} \frametitle{Copernicus Principle}
\framesubtitle{aka Mediocrity Principle}
\centering
{\large ``if an item is drawn at random from one of several sets or categories, it's likelier to come from the most numerous category than from any one of the less numerous categories''} \\
\vspace{0.3in}
\includegraphics[width=0.7\linewidth]{../figs/class6/M31.jpg}\\
{\tiny Credit: By Adam Evans - M31, CC BY 2.0}
\end{frame}

\begin{frame} \frametitle{More Learning from Little Data}
\centering
\emph{Standing since 2500 BCE, how much longer?}
\includegraphics[width=0.8\linewidth]{../figs/class6/pyramids.jpg}\\
{\tiny By Ricardo Liberato - All Gizah Pyramids, CC BY-SA 2.0}
\end{frame}

\begin{frame} \frametitle{More Learning from Little Data}
\centering
\emph{8 years old, life expectancy?}
\includegraphics[width=0.8\linewidth]{../figs/class6/8years.jpg}\\
{\tiny By Alvesgaspar - Own work, CC BY-SA 2.5}
\end{frame}

\begin{frame} \frametitle{More Learning from Little Data}
\centering
\emph{$8$ years old, life expectancy?}
\includegraphics[width=0.8\linewidth]{../figs/class6/8years.jpg}\\
{\tiny By Alvesgaspar - Own work, CC BY-SA 2.5}
\end{frame}

\begin{frame} \frametitle{More Learning from Little Data}
\centering
\emph{$\$800$ Balance, will I default?} \\
\includegraphics[width=0.5\linewidth]{../figs/class6/credit.jpg}\\
\end{frame}

\begin{frame} \frametitle{Defaulting as Machine Learning}
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
\node[block](data){\textbf{Dataset}: Balance $\$800$};
\node[block,below of=data](algo){ML Algorithm};
\node[block,below of=algo](hypo){\textbf{Hypothesis}: Default?};
\node[left of=hypo,xshift=-1.3cm](input) {No input};
\node[right of=hypo,xshift=2.5cm](output) {Target: Yes or No};
\path (data) edge (algo) 
(algo) edge (hypo)
(input) edge [dashed] (hypo)
(hypo) edge [dashed] (output);
\end{tikzpicture}
\end{frame}

\begin{frame} \frametitle{Machine Learning LDA}
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
\node[block](data){Dataset: Balance and defaults};
\node[block,below of=data](algo){ML Algorithm};
\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
\node[left of=hypo,xshift=-1cm](input) {Balance $\tcg{X}$};
\node[right of=hypo,xshift=1cm](output) {Default? $\tcb{Y}$};
\path (data) edge (algo) 
(algo) edge (hypo)
(hypo) edge [dashed] (input)
(output) edge [dashed] (hypo);
\node[block,right of=algo,xshift=1cm](prior){Class prior $\tcr{\pi}$};
\path (algo) edge (prior) 
(prior) edge [dashed] (output);	
\end{tikzpicture}\\[1cm]
\pause
\emph{Learning and prediction can be very similar}
\end{frame}

\begin{frame} \frametitle{Priors}
\centering
\emph{What do you believe with no data?} \\[1cm]

\begin{tabular}{|l|l|}
\hline
\textbf{Domain} & \textbf{Distribution} \\
\hline \hline
Number of buses & Power law \\
Longevity of structures & Power law \\
Human life span & Normal \\
Time in political office & Erlang \\
Coin bias & Beta \\
Die bias & Dirichlet \\
\hline
\end{tabular}
\end{frame}

\begin{frame}\frametitle{Bayes Theorem}
	\begin{itemize}
		\item Classification from label distributions:
		\[ \Pr[Y = k \mid X = x] = \frac{\Pr[X = x \mid Y = k] \Pr[Y = k]}{\Pr[X = x]} \]
		\item Example:
		\begin{gather*} \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance} = \$100] = \\ \frac{\Pr[\varname{balance} = \$100 \mid \varname{default} = \operatorname{yes}] \Pr[\varname{default} = \operatorname{yes}]}{\Pr[\varname{balance} = \$100]} 
		\end{gather*}
		\item Notation:
		\[ \Pr[Y = k \mid X = x] = \frac{\pi_k f_k(x)}{\sum_{l=1}^{K} \pi_l f_l(x)} \]
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Bayesian Maximum A Posteriori (MAP) }
	\begin{enumerate}
		\item \textbf{Maximum likelihood}		
		\[ \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
		\item \textbf{Maximum a posteriori estimate (MAP)}		
		\[  \max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \visible<2->{=  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}} \]
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Maximum a Posteriori Estimate}
	\[  
	\max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] =  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}
	\]
	\begin{itemize}
		\item \textbf{Prior}:
		\[  \Pr[\tcr{\operatorname{model}}] \] 
		\item \textbf{Posterior}:
		\[ \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \]
		\item \textbf{Likelihood}: 
		\[ \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{MAP vs Max Likelihood}
	Computed models are the \textbf{same} when:\\
	\vfill
	\begin{enumerate}
		\item<2-> Prior is uniform. \emph{Uninformative priors}
		\item<3-> Amount of data available is large / infinite 
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{MAP Advantages and Disadvantages}
\begin{itemize}
	\item \textbf{Advantages}: Can provide an informative \emph{prior}.
	\pause
	\vfill
	\item \textbf{Disadvantages}: Must provide a \emph{prior}. Where can we get it?
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Uninformative Priors}

\begin{enumerate}
	\item \emph{Two Envelopes Paradox}: Uninformative uniform priors can be dangerous! \\
	\url{https://en.wikipedia.org/wiki/Two_envelopes_problem}
\end{enumerate}
	
\end{frame}

\end{document}