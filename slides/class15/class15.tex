\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}

\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\tr}{^\top}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\newenvironment{mprog}{\begin{equation}\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}\end{equation}}
\newenvironment{mprog*}{\begin{equation*}\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}\end{equation*}}
\newcommand{\stc}{\\[1ex] \subjectto}
\newcommand{\subjectto}{\mbox{s.t.} &}
\newcommand{\minimize}[1]{\min_{#1} &}
\newcommand{\maximize}[1]{\max_{#1} &}
\newcommand{\minsep}[2]{\min_{#1 \; \vline \; #2} &}
\newcommand{\maxsep}[2]{\max_{#1 \; \vline \; #2} &}
\newcommand{\cs}{\\[1ex] & }

\title{Kernels and Computational Learning Theory}
\subtitle{Maximum Margin Classifiers}
\author{Marek Petrik}
\date{Apr 2nd, 2018}

\begin{document}
	
\begin{frame} \maketitle
\end{frame}


\begin{frame} \frametitle{Machine Learning Choices ...}
\centering
\includegraphics[width=\linewidth]{../figs/class1/ml_map.png}\\
{\tiny Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/index.html}} \\[3mm]
\end{frame}

\begin{frame} \frametitle{Separating Hyperplane}
	\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.1}.pdf}\end{center}
	\only<1>{Hyperplane: $\beta_0 + x\tr \beta = 0$}
	\only<2>{Blue: $\beta_0 + x\tr \beta > 0$}
	\only<3>{Red: $\beta_0 + x\tr \beta < 0$}	
\end{frame}

\begin{frame} \frametitle{Best Separating Hyperplane}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.2}.pdf}\end{center}
	\begin{itemize}
		\item Data is separable
		\item Why would either one be better than others?
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Maximum Margin Hyperplane}
	\begin{center}
	\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.3}.pdf}
	\end{center}
\end{frame}

\begin{frame} \frametitle{Non-separable Case}
	\begin{itemize}
		\item Rarely lucky enough to get separable classes
	\end{itemize}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter9/9.4}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Almost Unseparable Cases}
	\begin{itemize}
		\item Maximum margin can be brittle even when classes are separable
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.5}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Introducing Slack Variables}
	\begin{itemize}
		\item \textbf{Maximum margin classifier}
		\begin{mprog*}
			\maximize{\beta,M} \qquad M 
			\stc y_i (\beta\tr x_i) \ge M
			\cs \| \beta \|_2 = 1
		\end{mprog*}
		\item \textbf{Support Vector Classifier} a.k.a Linear SVM
		\begin{mprog*}
			\maximize{\beta,M, \epsilon\ge 0} \qquad M 
			\stc y_i (\beta\tr x_i) \ge (M - \alert{\epsilon_i})
			\cs \| \beta \|_2 = 1
			\cs \alert{\| \epsilon \|_1 \le C}
		\end{mprog*}	
		\item Slack variables: $\epsilon$
		\item Parameter: $C$ \visible<2>{What if $C=0$?}
	\end{itemize}	
\end{frame}

\begin{frame} \frametitle{Effect of Decreasing Parameter $C$}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter9/9.7}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{What About Nonlinearity?}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.8}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Dealing with Nonlinearity}
	\begin{itemize}
		\item Introduce more features, just like with logistic regression
		\item It is possible to do better with SVMs 
		\item \textbf{\underline{Primal} Quadratic Program}
		\begin{mprog*}
		\maximize{\beta,M} \qquad M 
		\stc y_i (\beta\tr x_i) \ge M
		\cs \| \beta \|_2 = 1
		\end{mprog*}
		\item Equivalent \textbf{\underline{Dual} Quadratic Program}  (usually max-min, not here)
		\begin{mprog*}
			\maximize{\alpha\ge 0} \sum_{l=1}^M \alpha_l-\frac{1}{2} \sum_{j,k=1}^{M} \alpha_j \alpha_k y_j y_k \langle x_j, x_k \rangle
			\stc \sum_{l=1}^M \alpha_l y_l = 0
		\end{mprog*}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{SVM Dual Representation}			
	\begin{itemize}		
		\item \textbf{\underline{Dual} Quadratic Program}  (usually max-min, not here)
		\begin{mprog*}
		\maximize{\alpha\ge 0} \sum_{l=1}^M \alpha_l-\frac{1}{2} \sum_{j,k=1}^{M} \alpha_j \alpha_k y_j y_k \alert<2>{\langle x_j, x_k \rangle}
		\stc \sum_{l=1}^M \alpha_l y_l = 0
		\end{mprog*}
		\item \textbf{Representer theorem}: (classification test): 
		\[f(z) = \sum_{l=1}^M \alpha_l y_l \alert<2>{\langle z, x_l\rangle} > 0\]
		\item<2-> Only need the inner product between data points
		\item<3-> Define a \textbf{kernel function} by projecting data to higher dimensions:
		\[ k(x_1,x_2) = \langle \phi(x_1), \phi(x_2) \rangle \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Kernelized SVM}
	\begin{itemize}
		\item \textbf{\underline{Dual} Quadratic Program} (usually max-min, not here)
		\begin{mprog*}
		\maximize{\alpha\ge 0} \sum_{l=1}^M \alpha_l-\frac{1}{2} \sum_{j,k=1}^{M} \alpha_j \alpha_k y_j y_k \alert{k(x_j, x_k )}
		\stc \sum_{l=1}^M \alpha_l y_l = 0
		\end{mprog*}
		\item \textbf{Representer theorem}: (classification test): 
		\[f(z) = \sum_{l=1}^M \alpha_l y_l \alert{k(z, x_l)} > 0\]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Kernels}
	\begin{itemize}
		\item Polynomial kernel: ($d$ is a coefficient)
		\[ k(x_1,x_2) = \left( 1 + x_1\tr x_2 \right)^d\]
		\item Radial kernel: ($\gamma$ is a coefficient)
		\[ k(x_1,x_2) = \exp\left( -\gamma \| x_1 - x_2 \|_2^2 \right) \]
		\item Many many more: Distance measure must be \textbf{positive definite}.
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Polynomial and Radial Kernels}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.9}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Polynomial Kernel}
	\[ k(x_1,x_2) = \left( 1 + x_1\tr x_2 \right)^d \]
	\begin{itemize}
		\item Common choice $d = 2$
		\item Recall that:  
		\[ k(x_1,x_2) = \langle  \phi(x_1), \phi(x_2) \rangle = \phi(x_1)\tr \phi(x_2) \]
		\item What is $\phi$ in a polynomial kernel?
		\pause
		\item Assume $2$ features: $x_{11}, x_{1,2}$
		\item Polynomial expansion of features:
		\[ \phi(x_1) = \begin{pmatrix}
		1 & 2 x_{11} & 2 x_{12} & x_{11}^2 & x_{12}^2 & x_{11} x_{12}
		\end{pmatrix} \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Other Popular Kernels}
	\begin{itemize}
		\item \textbf{Fisher kernel}: Motivated by statistical properties
		\item \textbf{String kernels}: Weighted sum of common substrings and other variations
		\item \textbf{Graph kernels}: Similarity between two graphs
		\item Problem specific kernels
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Custom Kernels}
	\begin{itemize}
		\item Can any function $k(x_1,x_2)$ be used as a kernel?
		\pause
		\item No. Two options.
		\pause
		\item \textbf{Option 1}: Positive semi-definite and must satisfy Mercer's conditions. Then:
		\[ k(x_1,x_2) = \langle  \phi(x_1), \phi(x_2) \rangle \]
		\item \textbf{Option 2:} Positive semi-definite only. SVM will work just fine, but for any $\phi$:
		\[ k(x_1,x_2) \neq \langle  \phi(x_1), \phi(x_2) \rangle \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{SVM vs LDA: Train}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.10}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{SVM vs LDA: Test}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.11}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Multiple Classes}
	\begin{itemize}
		\item One-vs-one
		\vfill
		\item One-vs-all
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{SVM vs Logistic Regression}
	\begin{itemize}
		\item \textbf{Logistic regression}: Minimize negative log likelihood
		\item \textbf{SVM}: Minimize \underline{hinge loss}
		\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.12}.pdf}\end{center}
	\end{itemize}
\textbf{Bottom line}: use SVM when classes are better separated or there is a good \emph{kernel}
\end{frame}

\begin{frame} \frametitle{Computational Learning Theory}

Read \textbf{Chapter 7} in Tom Mitchell's Machine Learning book. Ask for a copy.
\end{frame}

\end{document}