 \documentclass{beamer}
 
 
 \let\val\undefined
 \usepackage{pgf}
 \usepackage{pgfplots}
 \usepackage{tikz}
 \usepackage{booktabs}
 \usepackage{natbib}
 \usepackage{algorithm2e}
 \usepackage{siunitx}
 \usepackage{framed}
 \usepackage{longtable}
 \usepackage{amsmath}
 \usepackage{amsthm}
 \usepackage{bigdelim,multirow}
 \usepackage{nicefrac}
 
 \usepackage{libertine}
 
 \usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}
 
 % *** Styles ***
 \setbeamertemplate{navigation symbols}{}
 \usecolortheme{dolphin}
 %\usecolortheme{rose}
 \setbeamercovered{transparent}
 \usefonttheme{professionalfonts}
 %\usefonttheme[onlymath]{serif}
 
 % *** Colors ***
 \definecolor{maroon(x11)}{rgb}{0.69, 0.19, 0.38}
 \newcommand{\tc}[2]{\textcolor{#1}{#2}}
 \newcommand{\tcb}[1]{\tc{blue}{#1}}
 \newcommand{\tcr}[1]{\tc{red}{#1}}
 \newcommand{\tcg}[1]{\tc{green}{#1}}
 \newcommand{\tcm}[1]{\tc{maroon(x11)}{#1}}
 
 \def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 
 
 \newcommand{\Ex}{\mathbb{E}}
 %\newcommand{\Pr}{\mathbb{P}}
 \DeclareMathOperator{\Var}{Var}
 
 \newcommand{\plus}{\textcolor{green}{$\mathbf{+}$}}
 \newcommand{\minus}{\textcolor{red}{$\mathbf{-}$}}
 
 \definecolor{varcolor}{RGB}{132,23,49}
 \newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}
 
 \title{Midterm Review}
 \author{Marek Petrik}
 \date{March 5th, 2018}
 
 \begin{document}
 	
 	
\begin{frame}
 	\maketitle
 	\tiny{Some of the figures in these slides are taken from "An Introduction to Statistical Learning, with applications in R"  (Springer, 2013) with permission from the authors: G. James, D. Witten,  T. Hastie and R. Tibshirani}
 \end{frame}


\begin{frame} \frametitle{What is Machine Learning}
	\begin{itemize}
		\item Discover unknown function $\alert{f}$: 
		\[ \tcb{Y} = \tcr{f}(\tcg{X})  \]
		\item $\tcr{f}$ = \textbf{hypothesis}
		\item $\tcg{X}$ = \textbf{features}, or predictors, or inputs
		\item $\tcb{Y}$ = \textbf{response}, or target
	\end{itemize}	
	\[ \varname{MPG} = \alert{f}(\varname{Horsepower}) \]
	\begin{center}\includegraphics[width=0.5\linewidth]{../figs/class1/auto_function.pdf}\end{center}	
\end{frame}

\begin{frame} \frametitle{Machine Learning}
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
	\node[block](data){Dataset};
	\node[block,below of=data](algo){Machine Learning Algorithm};
	\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
	\node[left of=hypo,xshift=-1cm](input) {Predictors $\tcg{X}$};
	\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
	\path (data) edge (algo) 
	(algo) edge (hypo)
	(input) edge [dashed] (hypo)
	(hypo) edge [dashed] (output);
	\end{tikzpicture}
\end{frame}

\begin{frame} \frametitle{Errors in Machine Learning}
Must allow for errors $\epsilon$:
\[ \tcb{Y} = \tcr{f}(\tcg{X}) + \epsilon \]			
\pause
\begin{enumerate}
	\item World is too complex to model precisely
	\item Many features are not captured in data sets
	\item Datasets are limited
\end{enumerate}

\begin{center}
	\visible<2>{\alert{How to reduce prediction errors?}}
\end{center}
\end{frame}


\begin{frame} \frametitle{KNN: K-Nearest Neighbors}
\textbf{Idea}: Use several similar training points when making predictions. Errors will average out.
\begin{center}
Example with 2 features (horsepower, model year) \\
\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.14}.pdf}
\end{center}	
\end{frame}

\begin{frame} \frametitle{KNN: Effect of different $k$}
\begin{center}\includegraphics[width=\linewidth]{../figs/class1/auto_knn_compare.pdf}\end{center}	
\end{frame}

	\begin{frame}\frametitle{Why Estimate Hypothesis $f$?}
\[ \varname{Sales} = f(\varname{TV}, \varname{Radio}, \varname{Newspaper}) \]
\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter2/2.1}.pdf}\end{center}	
\begin{enumerate}
	\item<2-> \textbf{Prediction}: Make predictions about future: Best medium mix to spend ad money?
	\item<3-> \textbf{Inference}: Understand the relationship: What kind of ads work? Why?
\end{enumerate}
\end{frame}

	\begin{frame}\frametitle{Types of Function $f$}
\begin{columns}
	\begin{column}{0.5\linewidth}
		\centering
		\textbf{Regression}: continuous target
		\[ \tcr{f} : \tcg{\mathcal{X}} \rightarrow \tcb{\mathbb{R}} \]
		\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.6}.pdf}\end{center}	
	\end{column}
	\begin{column}{0.5\linewidth}
		\centering
		\textbf{Classification}: discrete target
		\[ \tcr{f} : \tcg{\mathcal{X}} \rightarrow \tcb{\{ 1,2,3,\ldots, k \}} \]
		\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.13}.pdf}\end{center}	
	\end{column}		
\end{columns}
\end{frame}

\begin{frame} \frametitle{Regression or Classification?}
\begin{small}
\begin{tabular}{|l|c|c|}
	\hline
	Application & Regression & Classification \\
	\hline
	Identify risk of getting a disease & \visible<2->{\checkmark} &  \\
	Predict effectiveness of a treatment & \visible<3->{\checkmark} &  \\
	Recognize hand-written text &  & \visible<4->{\checkmark} \\
	Speech recognition  &  & \visible<5->{\checkmark}\\
	Predict probability of an employee leaving & \visible<6->{\checkmark} &  \\
	\hline
\end{tabular}
\end{small}
\end{frame}


	\begin{frame}\frametitle{How Good are Predictions?}
\begin{enumerate}
	\item Split data into \textbf{train} and \textbf{test} sets
	\item Learn function $\hat{f}$ using only \textbf{training} data
	\item Test data: ${(x_1,y_1), (x_2, y_2), \ldots}$
	\item Compute \textbf{Mean Squared Error (MSE)} on \textbf{test} data: 
	\[ \varname{MSE} = \frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{f}(x_i))^2  \]
\end{enumerate}
\textbf{Important assumption}: Samples $x_i$ are i.i.d.
\end{frame}

\begin{frame} \frametitle{Do We Need Test Data?}
\begin{itemize}
\item Why not just test on the training data?
\begin{center}
	KNN Error
	\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.17}.pdf}	
\end{center}	
\item Small $k$ is flexible and overfits
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Why Methods Fail: Bias-Variance Decomposition}
\[ Y = f(X) + \alert{\epsilon} \]			
Mean Squared Error of trained $\hat{f}$ can be decomposed as:
\[ \varname{MSE} = \Ex [(Y - \hat{f}(X))^2] = \underbrace{\Var[\hat{f}(X)]}_{\text{\tcg{Variance}}} + \underbrace{\Ex[\hat{f}(X) - f(X)]^2}_{\text{\tcb{Bias}}} + \Var[\tcr{\epsilon}] \]
\begin{itemize}
	\item \textbf{\tcb{Bias}}: How well would method work with infinite data
	\item \textbf{\tcg{Variance}}: How much does output change with different data sets
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Bias-Variance Trade-off}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter2/2.12}.pdf}\end{center}
\end{frame}	

\begin{frame} \frametitle{Simple Linear Regression}
\begin{itemize}
	\item There is only one feature:
	\[ \tcr{Y} \approx \tcg{\beta_0} + \tcb{\beta_1} X \qquad \tcr{Y} = \tcg{\beta_0} + \tcb{\beta_1} X  + \epsilon \]
	\item Example:
	%\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter2/2.2}.pdf}\end{center}				
	\begin{center}\includegraphics[width=0.8\linewidth]{{../figs/class2/sales_tv_reg}.pdf}\end{center}				
	\[ \tcr{\textrm{Sales}} \approx \tcg{\beta_0} + \tcb{\beta_1} \times \varname{TV} \]				
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Correlation Coefficient}
\begin{itemize}
	\item Measures dependence between two random variables $X$ and $Y$ 
	\[ r(X) = \frac{\operatorname{Cov}(X,Y)}{\sqrt{\Var(X)}\sqrt{\operatorname{Var}(Y)}} \]
	\item Correlation coefficient $r$ is between $[-1,1]$
	\begin{description}
		\item[$0$:] Variables are not related
		\item[$1$:] Variables are perfectly  related (same)
		\item[$-1$:] Variables are negatively related (different)
	\end{description}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
\centering
\includegraphics[width=0.8\linewidth]{../figs/class2/students_example0.pdf} \\[3mm]
\visible<2>{Correlation: $0$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
\centering
\includegraphics[width=0.8\linewidth]{../figs/class2/students_example1.pdf} \\[3mm]
\visible<2>{Correlation: $1$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
\centering
\includegraphics[width=0.8\linewidth]{../figs/class2/students_example1_stretched.pdf} \\[3mm]
\visible<2>{Correlation: $1$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
\centering
\includegraphics[width=0.8\linewidth]{../figs/class2/students_example2.pdf} \\[3mm]
\visible<2>{Correlation: $-1$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
\centering
\includegraphics[width=0.8\linewidth]{../figs/class2/students_example3.pdf} \\[3mm]
\visible<2>{Correlation: $0.5$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
\centering
\includegraphics[width=0.8\linewidth]{../figs/class2/students_example4.pdf} \\[3mm]
\visible<2>{Correlation: $0.0$}
\end{frame}

\begin{frame}\frametitle{$R^2$ Statistic}		
\[ R^2 = 1 - \frac{\operatorname{RSS}}{\operatorname{TSS}} = 1 - \frac{\sum_{i=1}^n (\tcm{y_i} - \tcr{\hat{y}_i})^2 }{\sum_{i=1}^n (\tcm{y_i} - \tcb{\bar{y}})^2} \]
\begin{itemize}
\item RSS - residual sum of squares, TSS - total sum of squares
\item $R^2$ measures the goodness of the fit as a proportion
\item Proportion of data variance explained by the model
\item Extreme values:
\begin{description}
\item[$0$:] Model does not explain data
\item[$1$:] Model explains data perfectly
\end{description}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{$R^2$ Statistic and Correlation Coefficient $r$}
\visible<2->{\[ R^2 = r^2 \]}
\vfill
\visible<3->{
	Correlation coefficient $r$:
	\begin{description}
		\item[$0$:] Variables are not related, $R^2 = 0$
		\item[$1$:] Variables are perfectly  related (same), $R^2 = 1$
		\item[$-1$:] Variables are negatively related (different), $R^2 = 1$
\end{description}	}
\end{frame}


\begin{frame} \frametitle{Why Minimize RSS}
\begin{enumerate}
	\item<2-> Maximize likelihood when $Y = \beta_0 + \beta_1 X + \epsilon$ when $\epsilon \sim \mathcal{N}(0,\sigma^2)$
	\vfill
	\item<3-> Best Linear Unbiased Estimator (BLUE): Gauss-Markov Theorem (ESL 3.2.2)
	\vfill
	\item<4> It is convenient: can be solved in closed form
\end{enumerate}
\end{frame}


\begin{frame} \frametitle{Inference in Linear Regression}
\begin{center} ``Is only a subset of predictors useful?'' \end{center}
\begin{itemize}
	\item Compare prediction accuracy with only a subset of features
	\item<2-> \textbf{RSS always decreases with more features!}
	\item<3-> Other measures control for number of variables:
	\begin{enumerate}
		\item Mallows $C_p$
		\item Akaike information criterion
		\item Bayesian information criterion
		\item Adjusted $R^2$
	\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Qualitative Features: Indicator Variables}
\begin{itemize}
	\item Predict $\varname{salary}$ as a function of $\varname{state}$
	\item Feature $\varname{state}_i \in \{ \operatorname{MA}, \operatorname{NH}, \operatorname{ME} \}$
	\item<2-> Introduce 2 \textbf{indicator variables} $x_i, z_i$: 
	\[x_i = \begin{cases}
	0 & \text{if } \varname{state}_i = \operatorname{MA} \\
	1 & \text{if } \varname{state}_i \neq \operatorname{MA} \\
	\end{cases} \qquad
	z_i = \begin{cases}
	0 & \text{if } \varname{state}_i = \operatorname{NH} \\
	1 & \text{if } \varname{state}_i \neq \operatorname{NH} \\
	\end{cases}\]
	\item<2-> Predict salary as:
	\[ \varname{salary} = \beta_0 + \beta_1 \times x_i + \beta_2 \times z_i  = \begin{cases}
	\beta_0 + \beta_1 & \text{if } \varname{state}_i = \operatorname{MA} \\
	\beta_0 + \beta_2 & \text{if } \varname{state}_i = \operatorname{NH} \\
	\beta_0 		  & \text{if } \varname{state}_i = \operatorname{ME} \\
	\end{cases} \]
	\item<3-> \alert{Need an indicator variable for ME? Why?} hint: linear independence
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Correlated Errors}
\begin{itemize}
	\item The errors $\epsilon_i$ are not independent
	\item For example, use each data point twice 
	\item No additional information, but error is apparently reduced
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter3/3.10}.pdf}\end{center}%	
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Non-constant Variance of Errors}
\begin{itemize}
\item Errors $\epsilon_1, \epsilon_2, \ldots, \epsilon_n$
\item \textbf{Homoscedastic} errors: $\Var[\epsilon_1] = \Var[\epsilon_2] =  \ldots = \Var[\epsilon_n]$
\item \textbf{Heteroscedastic} errors can cause a wrong fit
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.11}.pdf}\end{center}%		
\item \textbf{Remedy}: scale response variable $Y$ or use \emph{weighted linear regression}	
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Outlier Data Points}
\begin{itemize}
\item Data point that is far away from others
\item Measurement failure, sensor fails, missing data point
\item Can seriously influence prediction quality
\end{itemize}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.12}.pdf}\end{center}%	
\end{frame}

\begin{frame}\frametitle{Points with High Leverage}
\begin{itemize}
\item Points with unusual value of $x_i$ 
\item Single data point can have significant impact on prediction 
\item R and other packages can compute leverages of data points
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.13}.pdf}\end{center}%	
\item Good to remove points with high leverage and residual
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Collinear Features}
\begin{itemize}
\item Collinear features can reduce prediction confidence
\[\varname{credit} \approx  \beta_0 + \beta_1 \times \varname{age} + \beta_2\times\varname{limit} \]
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.14}.pdf}\end{center}%	
\item Detect by computing feature correlations			
\item Solution: remove collinear feature or combine them
\end{itemize}
\end{frame}

	\begin{frame} \frametitle{Logistic Regression}
\begin{itemize}
	\item Predict \textbf{probability} of a class: $p(X)$
	\item Example: $p(\varname{balance})$ probability of default for person with $\varname{balance}$
	\item \textbf{Linear regression}:
	\[ p(X) = \beta_0 + \beta_1  \]
	\item \textbf{logistic regression}:
	\[ p(X) = \frac{e^{\beta_0+\beta_1 \, X}}{1+ e^{\beta_0 + \beta_1\,X}}  \]
	\item the same as:
	\[ \log\left( \frac{p(X)}{1-p(X)}\right) = \beta_0 + \beta_1\,X \]
	\item \underline{Odds}: $\nicefrac{p(X)}{1-p(X)}$
\end{itemize}
\end{frame}

	\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
\begin{itemize}
	\item \textbf{Likelihood}: Probability that data is generated from a model
	\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
	\item Find the most likely model:
	\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
	\item Likelihood function is difficult to maximize
	\item Transform it using $\log$ (strictly increasing)
	\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]
	\item Strictly increasing transformation does not change maximum
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Example: Maximum Likelihood}
\begin{itemize}
\item Assume a coin with $p$ as the probability of \emph{heads}
\item \textbf{Data}: \underline{$h$ heads, $t$ tails}
\item The likelihood function is:
\[ \ell(p) = {h+t \choose h} p^h \, (1-p)^t \approx p^h \, (1-p)^t~.\]
\begin{center}				
	\vspace{-7mm}
	\includegraphics[width=\linewidth]{../figs/class4/likelihood10.pdf}
\end{center} 
\end{itemize}
\end{frame}

	\begin{frame} \frametitle{Maximizing Likelihood}
\begin{itemize}
	\item Likelihood function is not concave: hard to maximize
	\[ \ell(p) = p^h \, (1-p)^t ~.\]
	\item Maximize the log-likelihood instead
	\[ \log \ell(p) = h\,\log(p) + t\, \log(1-p) ~.\]
	\begin{center}				
		\vspace{-9mm}
		\includegraphics[width=\linewidth]{../figs/class4/loglikelihood.pdf}
	\end{center} 
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Log-likelihood: Biased Coin}
\begin{center}
\textbf{heads} $h=20$ \qquad \textbf{tails} $t=50$\\				
\vspace{-5mm}
\includegraphics[width=\linewidth]{../figs/class4/loglikelihood_biased.pdf}
\end{center} 
\end{frame}

\begin{frame}\frametitle{Discriminative vs Generative Models}
\begin{itemize}
	\item \textbf{Discriminative models}
	\begin{itemize}
		\item Estimate conditional models $\Pr[Y \mid X]$ 
		\item Linear regression 
		\item Logistic regression
	\end{itemize}
	\vfill
	\item \textbf{Generative models}
	\begin{itemize}
		\item Estimate joint probability $\Pr[Y, X] = \Pr[Y \mid X] \Pr[X]$
		\item Estimates not only probability of labels but also the features
		\item Once model is fit, can be used to generate data
		\item LDA, QDA, Naive Bayes
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Discriminative Machine Learning}
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
\node[block](data){Dataset};
\node[block,below of=data](algo){ML Algorithm};
\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
\node[left of=hypo,xshift=-1cm](input) {Features $\tcg{X}$};
\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
\path (data) edge (algo) 
(algo) edge (hypo)
(input) edge [dashed] (hypo)
(hypo) edge [dashed] (output);
\end{tikzpicture}
\end{frame}

\begin{frame} \frametitle{Generative Machine Learning}
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
\node[block](data){Dataset};
\node[block,below of=data](algo){ML Algorithm};
\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
\node[left of=hypo,xshift=-1cm](input) {Features $\tcg{X}$};
\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
\path (data) edge (algo) 
(algo) edge (hypo)
(hypo) edge [dashed] (input)
(output) edge [dashed] (hypo);
\visible<2->{
\node[block,right of=algo,xshift=1cm](prior){Class prior $\tcr{\pi}$};
\path (algo) edge (prior) 
(prior) edge [dashed] (output);	
}
\end{tikzpicture}
\end{frame}

\begin{frame}\frametitle{Generative Models}
\begin{itemize}
\item[\plus] Can be used to generate data ($\Pr[X]$)
\item[\plus] Offers more insights into data
\item[\minus] Often works worse, particularly when assumptions are violated
\end{itemize}
\end{frame}

\begin{frame}\frametitle{LDA: Linear Discriminant Analysis}
\begin{itemize}
	\item \textbf{Generative model}: capture probability of predictors for each label		
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.4}.pdf}\end{center}	
	\item Predict:
	\begin{enumerate}
		\item<2-> $\Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ]$ and $\Pr[\varname{default} = \operatorname{yes}]$
		\item<3-> $\Pr[\varname{balance} \mid \varname{default} = \operatorname{no} ]$ and $\Pr[\varname{default} = \operatorname{no}]$
	\end{enumerate}
	\item<4-> Classes are normal: $\Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ]$
\end{itemize}
\end{frame}

\begin{frame} \frametitle{LDA vs Logistic Regression}
\begin{itemize}
\item \textbf{Logistic regressions}:
\[ \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] \]
\vfill
\item \textbf{Linear discriminant analysis}:
\[ \Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ] \text{ and } \Pr[\varname{default} = \operatorname{yes}] \]
\[ \Pr[\varname{balance} \mid \varname{default} = \operatorname{no} ] \text{ and } \Pr[\varname{default} = \operatorname{no}] \]
\end{itemize}	
\end{frame}

\begin{frame}\frametitle{LDA with 1 Feature}
\begin{itemize}
\item Classes are normal and class probabilities $\pi_k$ are scalars
\[ f_k(x) = \frac{1}{\sigma\sqrt{2\pi}} e^{  - \frac{1}{2\sigma^2} (x-\mu_k)^2 } \]
\item \textbf{Key Assumption}: Class variances $\sigma_k^2$	\underline{are the same}, means are the same.
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.4}.pdf}\end{center}	
\end{itemize}

\end{frame}

\begin{frame}\frametitle{Bayes Theorem}
\begin{itemize}
\item Classification from label distributions:
\[ \Pr[Y = k \mid X = x] = \frac{\Pr[X = x \mid Y = k] \Pr[Y = k]}{\Pr[X = x]} \]
\item<2-> Example:
\begin{gather*} \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance} = \$100] = \\ \frac{\Pr[\varname{balance} = \$100 \mid \varname{default} = \operatorname{yes}] \Pr[\varname{default} = \operatorname{yes}]}{\Pr[\varname{balance} = \$100]} 
\end{gather*}
\item<3-> Notation:
\[ \Pr[Y = k \mid X = x] = \frac{f_k(x) \pi_k}{\sum_{l=1}^{K} \pi_l f_l(x)} \]
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Classification With LDA}
$X: \varname{balance}$, $Y: \varname{default} \in \{ \operatorname{yes}, \operatorname{no}\}$
\begin{align*}
\text{Probability in class } \tcg{k_1} &> \text{Probability in class } \tcr{k_2} \\
\visible<2->{\Pr[Y = \tcg{k_1} \mid X = x] &> \Pr[Y = \tcr{k_2} \mid X = x] \\}
\visible<3->{\frac{\pi_{\tcg{k_1}} f_{\tcg{k_1}}(x)}{\sum_{l=1}^{K} \pi_l f_l(x)} &> \frac{\pi_{\tcr{k_2}} f_{\tcr{k_2}}(x)}{\sum_{l=1}^{K} \pi_l f_l(x)} \\}
\visible<4->{\pi_{\tcg{k_1}} f_{\tcg{k_1}}(x) &> \pi_{\tcr{k_2}} f_{\tcr{k_2}}(x) \\} 
\visible<5->{\log \left(\pi_{\tcg{k_1}} f_{\tcg{k_1}}(x) \right) &> \log\left(\pi_{\tcr{k_2}} f_{\tcr{k_2}}(x) \right) \\} 
\visible<6->{\hat{\delta}_{\tcg{k_1}}(x) &>  \hat{\delta}_{\tcr{k_2}}(x) }	
\end{align*}
\visible<6->{
\textbf{Discriminant function}: 
\[\hat{\delta}_{\tcb{k}}(x) = x\cdot \frac{\hat{\mu}_{\tcb{k}}}{\hat{\sigma}^2} - \frac{\hat{\mu}_{\tcb{k}}^2}{2\hat{\sigma}^2} + \log(\hat{\pi}_{\tcb{k}}) \]
\emph{Derive at home}
}
\end{frame}

\begin{frame}\frametitle{Multivariate Classification Using LDA}
\begin{itemize}
	\item \textbf{Linear}: Decision boundaries are linear
\end{itemize}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.6}.pdf}\end{center}	
\end{frame}

\begin{frame}\frametitle{QDA: Quadratic Discriminant Analysis}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.9}.pdf}\end{center}	
\end{frame}

\begin{frame}\frametitle{Confusion Matrix: Predict $\varname{default}$}
\begin{center}
	\begin{tabular}{l|l|c|c|c}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{True}} & \\ 
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\ 
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Yes & \tcg{$a$} & \tcr{$b$} & $a+b$\\   
		\cline{2-4}
		& No & \tcr{$c$} & \tcg{$d$} & $c+d$\\    
		\cline{2-4}
		\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$a+c$} & \multicolumn{    1}{c}{$b+d$} & \multicolumn{1}{c}{$N$}     
	\end{tabular}
\end{center}
\textbf{LDA}: Predict default if $\Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] > \nicefrac{1}{2}$
\begin{center}
	\begin{tabular}{l|l|c|c|c}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{True}} & \\ 
		\cline{3-4}
		\multicolumn{2}{c|}{} & Yes & No & \multicolumn{1}{c}{Total} \\ 
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Yes & \tcg{$81$} & \tcr{$23$} & $104$\\   
		\cline{2-4}
		& No & \tcr{$252$} & \tcg{$9\,644$} & $9\,896$\\    
		\cline{2-4}
		\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$333$} & \multicolumn{    1}{c}{$9\,667$} & \multicolumn{1}{c}{$10\,000$}     
	\end{tabular}
\end{center}
\begin{itemize}
	\item<2-> Good predictions?
	\item<3-> \textbf{No}: Most people who default are predicted as No default 
\end{itemize}
\end{frame}

\begin{frame}\frametitle{True Positives, etc}
\begin{center}
	\begin{tabular}{l|l|c|c|}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{Reality}}  \\ 
		\cline{3-4}
		\multicolumn{2}{c|}{} & Positive & Negative \\ 
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Positive & \tcg{True Positive} & \tcr{False Positive} \\   
		\cline{2-4}
		& Negative & \tcr{False Negative} & \tcg{True Negative} \\    
		\cline{2-4}
\end{tabular}\end{center}
\vspace{3mm}
\begin{itemize}
	\item \textbf{Recall/sensitivity} = TP/(TP+FN)
	\item \textbf{Precision} = TP/(TP+FP)
	\item \textbf{Specificity} = TN/(TN+FP)
\end{itemize}
\end{frame}

\begin{frame}\frametitle{ROC Curve}
\begin{center}
	\begin{tabular}{l|l|c|c|}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\textbf{Reality}}  \\ 
		\cline{3-4}
		\multicolumn{2}{c|}{} & Positive & Negative \\ 
		\cline{2-4}
		\multirow{2}{*}{\textbf{Predicted}}
		& Positive & \tcg{True Positive} & \tcr{False Positive} \\   
		\cline{2-4}
		& Negative & \tcr{False Negative} & \tcg{True Negative} \\    
		\cline{2-4}
	\end{tabular}
\end{center}
\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter4/4.8}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Bayesian Maximum A Posteriori (MAP) }
\begin{enumerate}
	\item \textbf{Maximum likelihood}		
	\[ \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
	\item \textbf{Maximum a posteriori estimate (MAP)}		
	\[  \max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \visible<2->{=  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}} \]
\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Maximum a Posteriori Estimate}
\[  
\max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] =  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}
\]
\begin{itemize}
\item \textbf{Prior}:
\[  \Pr[\tcr{\operatorname{model}}] \] 
\item \textbf{Posterior}:
\[ \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \]
\item \textbf{Likelihood}: 
\[ \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \]
\end{itemize}
\end{frame}

\begin{frame} \frametitle{MAP vs Max Likelihood}
Computed models are the \textbf{same} when:\\
\vfill
\begin{enumerate}
\item<2-> Prior is uniform. \emph{Uninformative priors}
\item<3-> Amount of data available is large / infinite 
\end{enumerate}
\end{frame}

\begin{frame} \frametitle{MAP Advantages and Disadvantages}
\begin{itemize}
\item \textbf{Advantages}: Can provide an informative \emph{prior}.
\pause
\vfill
\item \textbf{Disadvantages}: Must provide a \emph{prior}. Where can we get it?
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Bias-Variance 1: Validation Set}
\begin{itemize}		
	\item Just evaluate how well the method works on the test set
	\item \textbf{Randomly} split data to:
	\begin{enumerate}
		\item \underline{Training set}: about half of all data
		\item \underline{Validation set} (AKA hold-out set): remaining half
	\end{enumerate}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter5/5.1}.pdf}\end{center}
	\item<2-> Choose the number of features/representation based on minimizing error on \textbf{validation set}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Bias-Variance 2: Leave-one-out}
\begin{itemize}
	\item Addresses problems with validation set
	\item Split the data set into 2 parts:
	\begin{enumerate}
		\item \underline{Training}: Size $n-1$
		\item \underline{Validation}: Size $1$
	\end{enumerate}
	\item Repeat $n$ times: to get $n$ learning problems
	\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter5/5.3}.pdf}\end{center}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Bias-Variance 3: k-fold Cross-validation}
\begin{itemize}
	\item Hybrid between validation set and LOO
	\item Split training set into $k$ subsets
	\begin{enumerate}
		\item \underline{Training set}: $n - \nicefrac{n}{k} $
		\item \underline{Test set}: $\nicefrac{n}{k}$
	\end{enumerate}
	\item $k$ learning problems
	\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter5/5.5}.pdf}\end{center}	
	\item Cross-validation error:
	\[ \operatorname{CV}_{(k)} = \frac{1}{k} \sum_{i=1}^k \operatorname{MSE}_i \]
\end{itemize}
\end{frame}

\begin{frame}\frametitle{K in KNN}
\begin{itemize}
	\item How to decide on the right $k$ to use in KNN?
	\item<2-> Cross-validation!
	\begin{center}
		Logistic regression \hspace{30mm} KNN\\
		\vspace{-5mm}
		\includegraphics[width=\linewidth]{{../islrfigs/Chapter5/5.8}.pdf}
	\end{center}
	\begin{description}
		\item[Brown] Test error
		\item[Blue] Training error
		\item[Black] CV error
	\end{description}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Bootstrap}
\begin{itemize}
	\item \textbf{Goal}: Understand the confidence in learned parameters
	\item Most useful in inference
	\item How confident are we in learned values of $\beta$:
	\[ \varname{mpg} = \beta_0 + \beta_1 \, \varname{power} \]
	\item\textbf{Approach}: Run learning algorithm multiple times with different data sets:
	\item Create a new data-set by sampling \textbf{with replacement} from the original one
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Bootstrap Illustration}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter5/5.11}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Forward Stepwise Selection}
\begin{itemize}
	\item Greedy approximation of \emph{Best Subset Selection}
	\item Iteratively add more features
	\item Example: $\varname{credit} \sim \varname{rating} + \varname{income} + \varname{student} + \varname{limit}$		
\end{itemize}
\vspace{5mm}
\begin{algorithm}[H] \caption{Forward Stepwise Selection}
	$\mathcal{M}_0 \leftarrow $ \emph{null model} (no features) \;
	\For{$k =  \alert{0}, 1,2,\ldots, \alert{p-1}$}{
		Fit all $p - k$ models that augment $\mathcal{M}_k$ by one new feature \;
		$\mathcal{M}_{k+1} \leftarrow $ best of $p-k$ models according to a metric (CV error, $R^2$, etc)
	}
	\Return Best of $\mathcal{M}_0, \mathcal{M}_1, \ldots, \mathcal{M}_p$ according to metric above
\end{algorithm}
\begin{itemize}
	\item<2-> Complexity? \visible<3>{$O(p^2)$}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Regularization}
\begin{itemize}
	\item \textbf{Ridge regression} (parameter $\lambda$), $\ell_2$ penalty
	\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j \beta_j^2  =\\
	\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2  + \lambda \sum_j \beta_j^2
	\end{gather*}
	\item \textbf{Lasso} (parameter $\lambda$), $\ell_1$ penalty
	\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j |\beta_j| = \\
	\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2 + \lambda \sum_j |\beta_j|
	\end{gather*}
	\item Approximations to the $\ell_0$ solution
\end{itemize}	
\end{frame}

\begin{frame} \frametitle{Ridge Regression: Coefficient Values}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.4}.pdf}\end{center}	
\end{frame}

\begin{frame} \frametitle{Lasso: Coefficient Values}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.6}.pdf}\end{center}	
\end{frame}

\begin{frame} \frametitle{Why Lasso Works}
\begin{itemize}
	\item Bias-variance trade-off
	\item Increasing $\lambda$ increases bias
	\item Example: all features relevant
\end{itemize}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.8}.pdf}\\
	\textcolor{purple}{purple: test MSE}, black: bias, \textcolor{green}{green: variance} \\
	dotted (ridge) \end{center}	
\end{frame}

\begin{frame} \frametitle{Lasso Solutions are Sparse}
Constrained Lasso (left) vs Constrained Ridge Regression (right)
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.7}.pdf}\end{center}
Constraints are blue, red are contours of the objective
\end{frame}

\begin{frame} \frametitle{Standardizing Features}
\begin{itemize}
	\item Regularization and PCR depend on scales of features
	\item Good practice is to \emph{standardize} features to have \textbf{same variance}
	\[ \tilde{x}_{ij} = \frac{x_{ij}}{\sqrt{\frac{1}{n}\sum_{i=1}^n (x_{ij} - \bar{x}_j)^2}} \]
	\item Do not standardize features when they have the same units
\end{itemize}

\end{frame}

\end{document}