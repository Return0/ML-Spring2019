---
title: "Linear Algebra and Linear Regressions"
subtitle: "CS 780/880"
author: "Marek Petrik"
output: beamer_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(latex2exp)
```

## Simple Linear Regression

- Compute coefficients $\beta_0,\beta_1$:
$$ f(x) = \beta_0 + \beta_1 \cdot x $$

## Intercept as a Feature

- Represent intercept as a parameter for a 0-th feature that is always $1$.
$$ f(x) = \beta_0 \cdot 1 + \beta_1 \cdot x $$

- Notation $x_{i,j}$:
    - Data-point $i$
    - Feature $j$
- Intercept feature $x_{j,0} = 1$ for all $j$

## Two Data Points

- Data point features: $x_1, x_2$
- Targets $y_1,y_2$ 
- Parameters $\beta_0,\beta_1$ 

- Compute $\beta$s by solving a system of linear equations:
$$ 
\begin{aligned}
y_1 &= f(x_1) = \beta_0 \cdot x_{1,0} + \beta_1 \cdot x_{1,1} \\
y_2 &= f(x_2) = \beta_0 \cdot x_{2,0} + \beta_1 \cdot x_{2,1}
\end{aligned}
$$

## Matrix Representation

- Matrix representation:
$$ 
\underbrace{\begin{bmatrix} y_1 \\ y_2 \end{bmatrix}}_y  = \underbrace{\begin{bmatrix}  x_{1,0} &  x_{1,1} \\ x_{2,0} &  x_{2,1} \end{bmatrix}}_X
  \underbrace{\begin{bmatrix} \beta_0 \\ \beta_1 \end{bmatrix}}_\beta
$$
- $X$ is called the *design matrix* 
- Concise form:
    $$ y = X \beta $$
- Dimensions of $X$ are $N \times (K+1)$:
    - $K$ features 
    - $N$ data points
    
## Solving Linear Equations

- Compute the inverse matrix $X^{-1}$ to $X$
- Inverse satisfies ($I$ is the identity matrix):
$$ X^{-1} X = X X^{-1} = I $$
- Want to solve
 $$ y = X \beta $$
- Left-multiply both sides by $X^{-1}$:

$$ X^{-1} y = X^{-1} X \beta = \beta $$

## Example 1: Simple Simple Regression 

- Two data points and one feature:$x_{1,1} = 2$ and $x_{2,1} = 5$. 
- Design matrix becomes (don't forget the intercept feature):
```{r}
X <- rbind(c(1,2),
           c(1,5))
print(X)
```

- The targets are $y_1 = 7$ and $y_2 = 3$:
```{r}
y <- c(7,3)
print(y)
```


## Example 1: Plot 1

The two data points plotted look as follows:
```{r}
plot(X[,2],y,xlab=TeX("x"),ylab=TeX("y")); grid()
```

## Example 1: Algebraic Solution

- Invert the design matrix:
```{r}
Xinv <- solve(X)
print(Xinv)
```

## Example 1: Check inverse

- Double check it is indeed an inverse:
```{r}
print(Xinv %*% X)
print(X %*% Xinv)
```
- Yes the inverse works!


## Example 1: Algebraic Solution

- Compute $\beta$ coefficients:
```{r}
beta <- Xinv %*% y
print(beta)
```


## Example 1: Plot 2

- Plot the regression line:

```{r}
plot(X[,2],y,xlab=TeX("x"),ylab=TeX("y")); grid()
abline(beta[1], beta[2])
```

## Example 1: Validation

- Does the built-in linear regression give us the same result?
```{r}
lm(y ~ X[,2])$coeff
c(beta)
```
- Yes! The results are the same.

## Questions: 

  1. How can we compute the the parameters $\beta$ when there is only a single data point? 
  2. How about when there are more data points than features?


## Example 2: 4 Data Points

- Design matrix:
```{r}
X <- rbind(c(1,2),
           c(1,5),
           c(1,3),
           c(1,2))
y <- c(7,3,5,6)
```  

## Example 2: Plot

- Does not look promising:
```{r}
plot(X[,2],y); grid()
```

## Example 2: Solution

- Compute matrix inverse:
```{r}
tryCatch({solve(X)}, error = function(e){print(e)})
```
- Fails!
- What to do?

## Minimizing RSS

Recall that RSS is the residual sum of squares:
$$ \operatorname{RSS} = \sum_{i=1}^n (y_i - f(x_i))^2 $$

## Euclidean Norm

- More compact representation of RSS
- Euclidean or $L_2$ norm:

$$ \| z \|_2^2 = \sum_{i=1}^n z_i^2 = z^T z $$
- Also:

$$ \| z \|_2 = \sqrt{\sum_{i=1}^n z_i^2} $$

## RSS as $L_2$ Norm

- Linear algebra prediction:
$$ y = X \beta $$

- RSS can be written much more compactly:
$$ 
\begin{aligned}
\operatorname{RSS} &= \| y - X \beta \|_2^2 = \\
&= (y-X\beta)^T (y - X \beta) = \\
&= y^T y - 2 y^T X \beta + \beta^T X^T X \beta 
\end{aligned}
$$

## Minimizing RSS

- Linear regression chooses $\beta$ to minimize the RSS 
- Solve the following optimization problem:
$$ \min_\beta \| y - X \beta \|_2^2 $$
- Is this a convex minimization problem?
- How to to tell?

## Optimal Solution

- Set the gradient to 0:

$$ 
\begin{aligned}
\nabla_\beta \;\| y - X \beta \|_2^2 &= 0 \\
\nabla_\beta \; \Bigl( y^T y - 2 y^T X \beta + \beta^T X^T X \beta \Bigr) &= 0 \\
\nabla_\beta \; \Bigl( - 2 y^T X \beta + \beta^T X^T X \beta \Bigr) &= 0 \\
 - 2 X^T y + 2  X^T X \beta&= 0 \\
 X^T X \beta &= X^T y 
\end{aligned}
$$

## Linear Regression Implementation

- Solve:
$$ \beta = (X^T X)^{-1} X^T y$$
- What if $X$ is not a square matrix?

## Example 2: Solutions

- Solving:
$$ \beta = (X^T X)^{-1} X^T y$$
- Solve as a single line:
```{r}
beta <- solve(t(X) %*% X) %*% t(X) %*% y
print(beta)
```

- Can any square matrix be inverted?

## Example 2: Validation

To make sure that everything is OK, we should compare our implementation with the built-in linear regression.
```{r}
beta_in <- lm(y ~ X[,2])$coeff
print(beta_in)
print(beta)
```

## Example 2: Plot

And finally, the plot.
```{r}
plot(X[,2],y); grid()
abline(beta[1], beta[2])
```


## Numerical Issues

- Solving:
$$ \beta = (X^T X)^{-1} X^T y$$
- Do not compute matrix inverse if you can avoid it. Computing a matrix inverse is:

  1. *Slow*: There are faster ways of solving systems of linear equations
  2. *Unstable*: Linear algebra implementation if finite precision can lead to large errors for ill-conditioned matrices


## Example 3: Demo of Instability 

- Adjective: unstable, noun: instability
- Target and design matrix:

```{r}
set.seed(189)
y <- c(1,2,3,4,5,6)
X <- cbind(rep(1,6), 1.0*y, y+rnorm(6,sd=0.000001))
```

- What is the minimal RSS? What are the $\beta$s?
- What is special about these features?

## Example 3: Standard solution

- Use built-in linear regression: 
```{r}
lm(y ~ X[,1] + X[,2] + X[,3] - 1)$coeff
```

- Use our formula:
```{r}
solve(t(X) %*% X) %*% t(X) %*% y
```

## Example 3: Inverse Matrix

```{r}
solve(t(X) %*% X)
```

## Example 3: Condition Number

- Condition number of a matrix ($\sigma$ is a singular value):
$$ \kappa(A) = \frac{\sigma_{\max}}{\sigma_{\min}} $$
- Singular values:
```{r}
svd(t(X) %*% X)$d
```


## Different Approaches

- Other methods for computing
$$ \beta = (X^T X)^{-1} X^T y $$

1. Gaussian elimination
2. Cholesky decomposition 
3. QR decomposition
4. Many others (LU)

## Example 3: Gaussian Elimination

- Directly solve the system of linear equation. 
- Related to how a matrix inverse is often computed, but is faster by about a factor of $K$ and much more numerically stable.

```{r}
t(solve(t(X) %*% X, t(X) %*% y))
```

## Example 3: Cholesky decomposition (LDL) 

- Any *positive-definite* symmetric matrix $A = U^T U$ where $U$ is an **upper triangular** matrix.
- Triangular matrices are very easy and stable to invert. 
- Matrix $X^T X$ is symmetric and positive definite. 
- Compute the Cholesky decomposition of $X^T X$.

```{r}
U <- chol(t(X) %*% X)
U
```

## Example 3: Cholesky decomposition (LDL) 

- Validate:
```{r}
t(X) %*% X
t(U) %*% U
```

## Example 3: Cholesky decomposition (LDL) 

- Linear regression:
```{r}
chol2inv(U) %*% t(X) %*% y
```

## Example 3: QR decomposition 

- Any matrix can be decomposed to 
$$A = Q R$$ 
- $Q$ is an *orthogonal matrix* and $R$ is upper triangular. 
- Orthogonal matrix satisfies $Q^ Q = I$. 
- QR is also stable and fast. 
- No need to compute $X^T X$, compute the QR decomposition of $X$.
- When $X = Q R$ then
$$ X^T X = R^T Q^T Q R = R^T R$$

## Example 3: QR decomposition 

- $R$ matrix in the QR decomposition
```{r}
qr.R(qr(X))
```
- $R$ is the same as $U$ from Cholesky dec.:
```{r}
R <- qr.R(qr(X)) 
t(chol2inv(R) %*% t(X) %*% y)
```

- R's built-in linear algebra tools are bad. Use `RcppEigen` or `RcppArmadillo` for better results

## Column view of linear regression

- Linear regression is a computing linear combination of the columns. 
- Let $X_i$ be the vector that represent the feature $i$ for all samples. 
- Looking for linear combination of feature vectors that minimizes RSS.

$$ \min_\beta \| y - X_1 \beta_1 - \ldots - X_K \beta_K \|_2^2$$

## Example 1: Revisited

- Design matrix and target

```{r}
Xs <- rbind(c(1,2),
            c(1,5))
ys <- c(7,3)
```

## Example 1: Row View

- The goal is again to connect the two points using a line.

```{r}
plot(Xs[,2],ys,xlab=TeX("x"),ylab=TeX("y")); grid()
```

## Example 1: Column View

- Each feature is a *vector* (include the intercept). 
- Features *dashed* vectors  and target is *solid*.

```{r, fig.height=6}
plot(NULL, xlab="Sample 1", ylab="Sample 2", xlim=c(0,9), ylim=c(0,9)); grid();
arrows(0,0,Xs[1,1], Xs[2,1],lty=2); arrows(0,0,Xs[1,2], Xs[2,2],lty=2)
arrows(0,0,ys[1],ys[2])
```

## Linearly Dependent Features

- What is a linearly dependent feature?
- Linearly dependent feature will not reduce the RSS.  
```{r}
Xss <- cbind(Xs, c(3,6)); Xss
```

## Example 1a: Linearly Dependent Feature

```{r, fig.height=6}
plot(NULL, xlab="Sample 1", ylab="Sample 2", xlim=c(0,9), ylim=c(0,9)); grid();
arrows(0,0,Xss[1,1], Xss[2,1],lty=2); arrows(0,0,Xss[1,2], Xss[2,2],lty=2)
arrows(0,0,Xss[1,3], Xss[2,3],lty=2); arrows(0,0,ys[1],ys[2])
```

## Example 1a: Linearly Dependent Feature

- Linear regression:
```{r}
beta <- lm(ys ~ Xss - 1)$coeff
beta
```

## Example 1a: R Squared Impact

- Nope
```{r}
summary(lm(ys ~ Xs - 1))$r.squared
summary(lm(ys ~ Xss - 1))$r.squared
```

## Implementation of Other Methods

- LDA, QDA: Linear algebra, but more complicated derivation
- Logistic regression: Gradient descent + linear algebra
- KNN: K-d Trees to find neighbors efficiently
- Ridge regression: Homework!
- Lasso: Quadratic programming, Homotopy method (glmnet), Proximal Gradient Methods 
- *Perhaps*: Teaching a class on "Applied Optimization" next Fall.


